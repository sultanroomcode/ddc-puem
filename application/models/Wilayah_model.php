<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Wilayah_model extends CI_Model {

  // Get Provinsi
  public function getProvinsi(){

    $response = array();
 
    // Select record
    $this->db->select('*');
    $q = $this->db->get('drhidn_provinsi');
    $response = $q->result_array();

    return $response;
  }

  // Get Kota
  public function getKota($id_provinsi){

    $response = array();
 
    $this->db->select('*');
    $this->db->where('provinsi_id', $id_provinsi);
    $q = $this->db->get('drhidn_kota');
    $response = $q->result_array();

    return $response;
  }

  // Get kecamatan
  public function getKecamatan($id_kota){

    $response = array();
 
    // Select record
    $this->db->select('*');
    $this->db->where('kota_id', $id_kota);
    $q = $this->db->get('drhidn_kecamatan');
    $response = $q->result_array();

    return $response;
  }

  // Get Kelurahan
  public function getKelurahan($id_kecamatan){

    $response = array();
 
    // Select record
    $this->db->select('*');
    $this->db->where('kecamatan_id', $id_kecamatan);
    $q = $this->db->get('drhidn_kelurahan');
    $response = $q->result_array();

    return $response;
  }

  

}