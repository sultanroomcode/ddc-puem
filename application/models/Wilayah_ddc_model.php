<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Wilayah_ddc_model extends CI_Model {
  public $db2;
  public function __construct()
  {
    parent::__construct();
    $this->db2 = $this->load->database('db2', true);
  }

  // Get Kota
  public function getKota(){

    $response = array();
 
    $this->db2->where('type', 'kabupaten');
    $q = $this->db2->get('user');
    $response = $q->result_array();

    return $response;
  }

  // Get kecamatan
  public function getKecamatan($id_kota){

    $response = array();
 
    // Select record
    $this->db2->where('type','kecamatan');
    $this->db2->like('id', $id_kota, 'after'); 
    $q = $this->db2->get('user');
    $response = $q->result_array();

    return $response;
  }

  // Get Kelurahan
  public function getKelurahan($id_kecamatan){

    $response = array();
 
    // Select record
    $this->db2->where('type','desa');
    $this->db2->like('id', $id_kecamatan, 'after'); 
    $q = $this->db2->get('user');
    $response = $q->result_array();

    return $response;
  }

  public function getDesa($id_desa){
    $response = array();
 
    // Select record
    $this->db2->where('type','desa');
    $this->db2->where('id',$id_desa);
    $q = $this->db2->get('user');
    $response = $q->row_array();

    return $response;  
  }
}