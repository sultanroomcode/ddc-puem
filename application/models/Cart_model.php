<?php 

/**
 * Home_model
 */

class Cart_model extends CI_model
{
	//proses membeli
	public $db2;
	public function __construct()
	{
		parent::__construct();
		$this->db2 = $this->load->database('db2', true);
	}

	public function memberBuy() 
	{
		$datetime = date('Y-m-d H:i:s');
		$data = [
			"idp" => $this->input->post('idp', true),
			"kd_desa" => $this->input->post('kd_desa', true),
			"kd_toko" => $this->input->post('kd_toko', true),
			"nama" => $this->input->post('nama_produk', true),
			"foto_produk" => $this->input->post('foto_produk', true),
			"harga" => $this->input->post('harga', true),
			"kuantitas" => $this->input->post('kuantitas', true),
			"member_id" => $this->input->post('member_id', true),
			"created_by" => $this->input->post('member_id', true),
			"updated_by" => $this->input->post('member_id', true),
			"created_at" => $datetime,
			"updated_at" => $datetime,
		];
		$this->db->insert('member_buy', $data);
	}

	//proses mendapatkan transaksi berdasarkan ID
	public function getDetailTransaksi()
	{
		return $this->db->get('member_buy')->result_array();
	}

	//proses mendapatkan list transaksi berdasarkan user
	public function getListTransaksi($member_id)
	{
		$this->db->where('member_id', $member_id);
		return $this->db->get('member_buy')->result_array();
	}

	public function getTokoByIdProduct($idp)
	{
		$this->db2->where('puem_store_product.idp', $idp);
		$this->db2->join('puem_store', 'puem_store_product.kd_desa = puem_store.kd_desa AND puem_store_product.kd_toko = puem_store.kd_toko');
		$this->db2->join('puem_store_contact', 'puem_store_contact.kd_desa = puem_store.kd_desa AND puem_store_contact.kd_toko = puem_store.kd_toko');
		return $this->db2->get('puem_store_product')->result_array();
	}
}