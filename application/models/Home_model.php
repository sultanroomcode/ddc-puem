<?php 

/**
 * Home_model
 */

class Home_model extends CI_model
{
	public $db2;
	public function __construct()
	{
		parent::__construct();
		$this->db2 = $this->load->database('db2', true);
	}
	// User ---------------------------------------------------------------
	public function getAllUser()
	{
		return $this->db2->get('user')->result_array();
	}


	// Store / toko ---------------------------------------------------------------
	public function getAllStore()
	{
		return $this->db2->get('puem_store')->result_array();
	}


	// Produk ---------------------------------------------------------------
	public function getAllProduk()
	{
		$this->db2->limit(18);
		return $this->db2->get('puem_store_product')->result_array();
	}

	public function getSearchProduk($term)
	{
		$this->db2->like('nama', $term);
		return $this->db2->get('puem_store_product')->result_array();
	}
	// public function getProdukById($idp)
	// {
	// 	$this->db2->where(['idp' => $idp]);
	// 	return $this->db2->get('puem_store_product')->row_array();
	// }
	public function addReadOne($idp)
	{
		$this->db2->where('idp', $idp);
		$this->db2->set('jml_view', '`jml_view`+ 1', FALSE);
		$this->db2->update('puem_store_product');
	}
	public function getProdukPopular()
	{
		$this->db2->order_by('jml_view', 'DESC');
		$this->db2->limit(10);
		return $this->db2->get('puem_store_product')->result_array();
	}

	public function getProdukById($idp)
	{
		$this->db2->select('*');
		$this->db2->from('puem_store_product');
		$this->db2->join('puem_store', 'puem_store_product.kd_desa = puem_store.kd_desa');
		$this->db2->where(['idp' => $idp]);
		return $this->db2->get()->row_array();
	}


	// Photo ---------------------------------------------------------------
	public function getAllPhoto($id_product)
	{
		$this->db2->where(['idp' => $id_product]);
		return $this->db2->get('puem_store_product_photo')->result_array();
	}

	// Member ---------------------------------------------------------------
	public function getMemberById($kd_desa,$kd_toko)
	{
		$this->db2->where(['kd_desa' => $kd_desa, 'kd_toko' => $kd_toko]);
		return $this->db2->get('puem_store')->row_array();
	}

	public function getAllProdukByMember($kd_desa,$kd_toko)
	{
		$this->db2->select('*');
		$this->db2->from('puem_store_product');
		$this->db2->join('puem_store', 'puem_store_product.kd_toko = puem_store.kd_toko AND puem_store_product.kd_desa = puem_store.kd_desa');
		$this->db2->where(['puem_store_product.kd_desa' => $kd_desa, 'puem_store.kd_toko' => $kd_toko]);
		return $this->db2->get()->result_array();
	}

	public function getAllProdukByKategori($kategori)
	{
		$this->db2->select('*');
		$this->db2->from('puem_store_product');
		$this->db2->join('puem_store', 'puem_store_product.kd_toko = puem_store.kd_toko AND puem_store_product.kd_desa = puem_store.kd_desa');
		$this->db2->join('puem_product_kategori_taxo', 'puem_product_kategori_taxo.idp = puem_store_product.idp');
		$this->db2->join('puem_product_kategori', 'puem_product_kategori_taxo.slug = puem_product_kategori.slug');

		$this->db2->where(['puem_product_kategori_taxo.slug' => $kategori]);
		return $this->db2->get()->result_array();
	}

	public function getAllKategoriByProduk($produk_id)
	{
		$this->db2->select('*');
		$this->db2->from('puem_product_kategori_taxo');
		$this->db2->join('puem_product_kategori', 'puem_product_kategori.slug = puem_product_kategori_taxo.slug');
		$this->db2->where(['idp' => $produk_id]);
		
		return $this->db2->get()->result_array();
	}

	// Contact ---------------------------------------------------------------
	public function getContactById($kd_desa,$kd_toko)
	{
		$this->db2->where(['kd_desa' => $kd_desa, 'kd_toko' => $kd_toko]);
		return $this->db2->get('puem_store_contact')->result_array();
	}
	public function getMemberAll()
	{
		return $this->db2->get('puem_store')->result_array();
	}

	//pelatihan
	public function getPelatihanAll()
	{
		$this->db2->order_by('created_at', 'DESC');
		return $this->db2->get('puem_pelatihan')->result_array();
	}

	public function getSidebarPelatihan()
	{
		$this->db2->limit(10);
		$this->db2->order_by('created_at', 'DESC');
		return $this->db2->get('puem_pelatihan')->result_array();
	}

	public function getPelatihanById($id)
	{
		$this->db2->where(['id' => $id]);
		return $this->db2->get('puem_pelatihan')->row_array();
	}

	public function getVideoAll()
	{
		$this->db2->order_by('created_at', 'DESC');
		return $this->db2->get('puem_video')->result_array();
	}

	public function getKategoriAll()
	{
		return $this->db2->get('puem_product_kategori')->result_array();
	}

	public function getAllProdukByIdDesa($id_desa)
	{
		$this->db2->limit(18);
		$this->db2->where('kd_desa', $id_desa);

		return $this->db2->get('puem_store_product')->result_array();
	}

	public function getGaleriAll()
	{
		$this->db2->order_by('created_at', 'DESC');
		return $this->db2->get('puem_galeri')->result_array();
	}
}