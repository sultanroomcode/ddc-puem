<?php  

/**
 * Member
 */
class Member_model extends CI_model {

	
	// public function getAllMember ()
	// {
	// 	$this->db->join('user_access', 'admin.user_access = user_access.id');
	// 	return $this->db->get('admin')->result_array();
	// }

	public function getJumlahBuy($idp)
	{
		$this->db->select('COUNT(idp) as total');
		$this->db->where('idp', $idp);
		$this->db->group_by('idp');
		return $this->db->get('member_buy')->row_array();
	}
						

	function setToActive($email)
	{
		$this->db->set('is_aktif', 1);
		$this->db->where('email', $email);
		return $this->db->update('member');
    }

    function deleteTokenByEmail($email)
	{
		$this->db->where('email', $email);
		return $this->db->delete('member_token');
    }

    

    function verifyIt($token)
	{
        $this->db->where(['token' => $token]);
		return $this->db->get('member_token')->row_array();
    }

    function member($email)
	{
        $this->db->where(['email' => $email]);
		return $this->db->get('member')->row_array();
    }

	public function tambahDataMember() 
	{	
		$data = [
			"nama" => htmlspecialchars($this->input->post('nama', true)),
			"email" => htmlspecialchars($this->input->post('email', true)),
			"password" => md5($this->input->post('password1', true)),
			"level" => $this->input->post('level', true),
			"image" => $this->input->post('image', true),
			"user_access" => $this->input->post('user_access', true),
			"is_aktif" => $this->input->post('is_aktif', true)
		];
		$this->db->insert('member', $data);
	}

	//cek nip dan password member
    function auth_member($email,$password){
        $query=$this->db->query("SELECT * FROM member WHERE email='$email' AND password=MD5('$password') LIMIT 1");
        return $query;
    }

    function check_member_detail($id){
        $this->db->where(['id' => (int) $id]);
		return $this->db->get('member_detail')->row_array();
    }

    function member_detail($email){
        $this->db->where(['email' => $email]);
		return $this->db->get('member_detail')->result_array();
    }
    //event
    public function getPelatihanStatus($acara_id, $member_id)
    {
    	$this->db->where(['acara_id' => $acara_id, 'member_id' => $member_id]);
		return $this->db->get('member_register_event')->row_array();
    }

    public function tambahDataRegistrasi()
    {
    	$data = [
			"acara_id" => $this->input->post('acara_id', true),
			"member_id" => $this->input->post('member_id', true),
			"tanggal_daftar" => date('Y-m-d H:i:s')
		];
		$this->db->insert('member_register_event', $data);
    }

    public function tambahDataProfil() 
	{
		$data = [
			"id" => $this->input->post('id', true),
			"nama" => $this->input->post('nama', true),
			"alamat" => $this->input->post('alamat', true),
			"provinsi" => $this->input->post('provinsi', true),
			"kota" => $this->input->post('kota', true),
			"kecamatan" => $this->input->post('kecamatan', true),
			"kelurahan" => $this->input->post('kelurahan', true),
			"tempat_lahir" => $this->input->post('tempat_lahir', true),
			"tgl_lahir" => $this->input->post('tgl_lahir', true),
			"jenis_kelamin" => $this->input->post('jenis_kelamin', true),
			"agama" => $this->input->post('agama', true),
			"pekerjaan" => $this->input->post('pekerjaan', true),
			"email" => $this->input->post('email', true),
			"telepon" => $this->input->post('telepon', true)
		];
		$this->db->insert('member_detail', $data);
	}

	public function ubahDataProfil() 
	{
		$data = [
			"alamat" => $this->input->post('alamat', true),
			"tempat_lahir" => $this->input->post('tempat_lahir', true),
			"tgl_lahir" => $this->input->post('tgl_lahir', true),
			"jenis_kelamin" => $this->input->post('jenis_kelamin', true),
			"agama" => $this->input->post('agama', true),
			"pekerjaan" => $this->input->post('pekerjaan', true),
			"telepon" => $this->input->post('telepon', true)
		];
		$this->db->where('id', $this->input->post('id'));
		$this->db->update('member_detail', $data);
	}
 
    //cek nim dan password mahasiswa
    // function auth_member($username,$password){
    //     $query=$this->db->query("SELECT * FROM member WHERE username='$username' AND password=MD5('$password') LIMIT 1");
    //     return $query;
    // }

    // function auth_member($username,$password){
    //     $query=$this->db->query("SELECT * FROM member WHERE username='$username' AND password=MD5('$password') LIMIT 1");
    //     return $query;
	// }
	
	// EMAIL ACTIVATION
	public function tambahDataToken($token)
	{
		$user_token = [
			'email' => $this->input->post('email', true),
			'token' => $token,
			'date_created' => time()
		];
		return $this->db->insert('member_token', $user_token);
	}

	public function testEmail($emailclient)
	{
		$config = $this->config->item('email_smtp');//email smtp is in local configuration
		$headerconf = $this->config->item('email_smtp_header');

		$this->load->library('email', $config);
		$this->email->initialize($config);

		$this->email->from($headerconf['email_from'], $headerconf['email_name']);
		$this->email->to($emailclient);

		$this->email->subject('Testing Email');
		$this->email->message('Ini cuma email testing dari '.$headerconf['email_name']);

		if ($send = $this->email->send()) {
			return $send;
			
		} else {
			echo $this->email->print_debugger();
			die;
		}
	}

	public function _sendEmail($token, $type)
	{
		$config = $this->config->item('email_smtp');//email smtp is in local configuration
		$headerconf = $this->config->item('email_smtp_header');

		$this->load->library('email', $config);
		$this->email->initialize($config);

		$this->email->from($headerconf['email_from'], $headerconf['email_name']);
		$this->email->to($this->input->post('email'));

		if ($type == 'verify') {
			$this->email->subject('Verifikasi Akun');
			$this->email->message('Klik link ini untuk melakukan verifikasi : 
				<a href="'.base_url().'member/verify?email=' . $this->input->post('email') . '&token=' . urlencode($token) . '">Aktivasi</a>');
		}


		if ($this->email->send()) {
			return true;
			
		} else {
			echo $this->email->print_debugger();
			die;
		}
	}

	public function getPeserta()
	{
		return $this->db->get('member_register_event')->result_array();
	}

	public function getPesertaAcara($id)
	{
		$this->db->join('member_detail', 'member_detail.id = member_register_event.member_id');
		$this->db->where('member_register_event.acara_id', $id);
		return $this->db->get('member_register_event')->result_array();
	}

	public function getAcaraByPeserta($id)
	{
		$db2 = $this->load->database('db2', true);
		$this->db->join($db2->database.'.puem_pelatihan', 'puem_pelatihan.id = member_register_event.acara_id');
		$this->db->where('member_register_event.member_id', $id);
		return $this->db->get('member_register_event')->result_array();
	}
}