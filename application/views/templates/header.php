<!DOCTYPE html>
<html class="wide wow-animation" lang="en">
  <head>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <!-- <script async src="https://www.googletagmanager.com/gtag/js?id=UA-137483781-2"></script>
    <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-137483781-2');
    </script> -->
    <!-- Site Title-->
    <title><?= $judul; ?></title>
    <meta charset="utf-8">
    <meta name="format-detection" content="telephone=no">
    <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <link rel="icon" href="<?= base_url(); ?>assets/image/logo-provinsi-jawa-timur.png" type="image/x-icon">
    <!-- Stylesheets-->
    <!-- <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Montserrat:400,700%7CLato:400,700'"> -->
    <!-- <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900,300italic"> -->
    <link rel="stylesheet" href="<?= base_url(); ?>assets/tema/css/style.css">
    <link rel="stylesheet" href="<?= base_url(); ?>assets/tema/slick-1.8.1/slick/slick.css">
    <link rel="stylesheet" href="<?= base_url(); ?>assets/tema/slick-1.8.1/slick/slick-theme.css">
    <link rel="stylesheet" href="<?= base_url(); ?>assets/tema/datepicker/css/bootstrap-datepicker.min.css">
    <link rel="stylesheet" href="<?= base_url(); ?>assets/tema/image-zoom/css/zoom.css">
    <link rel="stylesheet" href="<?= base_url(); ?>assets/js/jquery-ui-1.12.1/jquery-ui.min.css">
    <link rel="stylesheet" href="<?= base_url(); ?>assets/js/tipped/dist/css/tipped.css">
    <link rel="stylesheet" href="<?= base_url(); ?>assets/css/custom.css">
    <!--[if lt IE 10]>
    <div style="background: #212121; padding: 10px 0; box-shadow: 3px 3px 5px 0 rgba(0,0,0,.3); clear: both; text-align:center; position: relative; z-index:1;"><a href="http://windows.microsoft.com/en-US/internet-explorer/"><img src="<?= base_url(); ?>vendor/tema/images/ie8-panel/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today."></a></div>
    <script src="js/html5shiv.min.js"></script>
    <![endif]-->
  </head>

  <body data-url="<?= base_url() ?>">
    <!-- <div class="preloader page-loader page-loader-variant-1">
      <div class="spinner"></div>
    </div> -->
    <!-- Page-->
    <div class="page">
      <header class="page-head">
        <!-- RD Navbar Transparent-->
        <div class="rd-navbar-wrap">
          <nav class="rd-navbar rd-navbar-top-panel novi-background rd-navbar-light" data-lg-stick-up-offset="79px" data-layout="rd-navbar-fixed" data-sm-layout="rd-navbar-fixed" data-md-device-layout="rd-navbar-fixed" data-lg-device-layout="rd-navbar-static" data-lg-stick-up="true" data-lg-auto-height="true" data-md-layout="rd-navbar-fixed" data-lg-layout="rd-navbar-static">
            <div class="container-fluid">
              <div class="rd-navbar-inner">
                <div class="rd-navbar-top-panel">
                  <?php if($this->session->ses_nama != null): ?>
                  <?= $this->session->ses_nama; ?>

                  | <span class="text-info"><i class="fa fa-user"></i> <a href="<?= base_url().'member/profile' ?>">Profil</a></span>
                  | <span class="text-info"><i class="fa fa-user"></i> <a href="<?= base_url().'member/list_acara' ?>">Acara</a></span>
                  | <span class="text-info"><i class="fa fa-user"></i> <a href="<?= base_url().'member/buy_history' ?>">Histori Belanja</a></span>
                  <?php endif; ?>
                  <div class="right-side">
                    <address class="contact-info text-left"><span class="p"><span class="novi-icon icon mdi mdi-city mb-2"></span>DPMD Prov. Jatim - <small class="text-success">Garasi PUEM</small></span><span class="p"><span class="novi-icon icon mdi mdi-phone"></span>(031) 8292591</span></address>
                  </div>
                </div>
                <!-- RD Navbar Panel-->
                <div class="rd-navbar-panel">
                  <div class="">
                    <div class="">
                      <!-- RD Navbar Toggle-->
                        <button class="rd-navbar-toggle" data-rd-navbar-toggle=".rd-navbar, .rd-navbar-nav-wrap"><span></span></button>
                        <!-- RD Navbar Top Panel Toggle-->
                        <button class="rd-navbar-top-panel-toggle" data-rd-navbar-toggle=".rd-navbar, .rd-navbar-top-panel"><span></span></button>
                        <!--Navbar Brand-->
                        <div class="rd-navbar-brand pb-2"><a href="<?= base_url(); ?>"><img width='150' height='32' src='<?= base_url(); ?>assets/image/logo-provinsi-jawa-timur2.png' alt=''/></a></div>
                    </div>
                    <!-- <div class="">
                       <div class="input-group input-group-sm mb-3">
                      <input type="text" class="form-control" placeholder="Recipient's username" aria-label="Recipient's username" aria-describedby="basic-addon2">
                      <div class="input-group-append">
                        <button class="btn btn-outline-secondary" type="button">Cari</button>
                      </div>
                      </div>
                    </div> -->
                  </div>
                </div>
                <div class="rd-navbar-menu-wrap">
                  <div class="rd-navbar-nav-wrap">
                    <div class="rd-navbar-mobile-scroll">
                      <!--Navbar Brand Mobile-->
                      <div class="rd-navbar-mobile-brand"><a href="<?= base_url(); ?>"><img width='230' height='38' src='<?= base_url(); ?>assets/image/logo-provinsi-jawa-timur2.png' alt=''/></a></div>
                      <!-- RD Navbar Nav-->
                      <ul class="rd-navbar-nav">
                        <li><a href="<?= base_url('home/about'); ?>"><span>Tentang PUEM</span></a></li>
                        <li><a href="#"><span>Kategori</span></a>
                          <div class="rd-navbar-megamenu">
                            <div class="row">
                              <ul class="col-xl-3">
                              <?php $model = $this->Home_model->getKategoriAll();
                              $i = 1;
                                foreach($model as $v):
                                ?>
                                <li><a href="<?= base_url().'kategori/'.$v['slug'] ?>#produk"><span class="rd-navbar-icon"></span><span class="text-middle"> <?= $v['label'] ?></span></a></li>
                                <?php 
                                if($i % 3 == 0){
                                  echo ' </ul>
                              <ul class="col-xl-3">';
                                }
                                $i++;
                                endforeach;
                                ?>
                              </ul>
                            </div>
                          </div>
                        </li>
                        <li><a href="<?= base_url('pelatihan'); ?>"><span>Pelatihan</span></a></li>
                        <li><a href="<?= base_url('video'); ?>"><span>Video</span></a></li>
                        <li><a href="<?= base_url('panduan'); ?>"><span>Panduan Belanja</span></a></li>
                        <li><a href="<?= base_url('galeri'); ?>"><span>Galeri</span></a></li>
                          <div class="button-header">
                            <div class="row px-1">
                              <div class="col-md-7">
                                <!-- pencarian -->
                                <div class="input-group input-group-sm">
                                  <input type="text" class="form-control" placeholder="cari produk .." autocomplete="off" size="40" autofocus="true" aria-label="Recipient's username" aria-describedby="basic-addon2" name="search-produk" id="search-produk">
                                </div>
                              </div>
                              <div class="col-md-5 mt-1">
                                <!-- button login / daftar -->
                                <?php if($this->session->akses !== NULL): ?>
                                  <a href="" class="btn btn-success btn-xs text-white" data-toggle="modal" data-target="#logoutModal"><span>Logout <i class="fa fa-sign-in"></i></span></a>

                                  <?php else: ?>

                                  <a href="<?= base_url('member/login'); ?>" class="btn btn-info btn-xs text-white"><span>Login <i class="fa fa-sign-in"></i></span></a>
                                  <a href="<?= base_url('member/daftar'); ?>" class="btn btn-outline-info btn-xs"><span>Daftar <i class="fa fa-user"></i></span></a>
                                  <?php endif; ?>
                              </div>
                            </div>
                          </div>
                      </ul>
                      
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </nav>
        </div>
      </header>


  <!-- Modal login -->
  <!-- <div class="modal fade mt-3" id="ModalLogin" tabindex="-1" role="dialog" aria-labelledby="ModalLogin" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="text-center" id="ModalLogin"></h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <h3 class="text-center"><b>LOGIN</b></h3>
          <div class="card-body">
          <form action="<?= base_url(); ?>member/auth" method="post" class="mb-4">
            <div class="form-group">
              <label for="email">Email</label>
              <input type="text" class="form-control" id="email" name="email" required="true">
            </div>
            <div class="form-group">
              <label for="password">Password</label>
              <input type="password" class="form-control" id="password" name="password" required="true">
            </div>
            <br>
            <button type="submit" class="btn btn-primary btn-block btn-sm" >Login <i class="fa fa-sign-in"></i></button>
             <p>Belum punya Akun ?  <a href="" class="text-info" data-dismiss="modal" data-toggle="modal" data-target="#ModalDaftar">Daftar Sekarang</a></p>
         </form>
       </div>
        </div>
      </div>
    </div>
  </div> -->

  <!-- Modal Daftar -->
  <!-- <div class="modal fade mt-3" id="ModalDaftar" tabindex="-1" role="dialog" aria-labelledby="ModalDaftar" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="text-center" id="ModalDaftar"></h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <h3 class="text-center"><b>DAFTAR</b></h3>
          <div class="card-body">
            <form action="<?= base_url('member/daftar'); ?>" method="post">
              <input type="hidden" name="image" value="default.png">
              <input type="hidden" name="level" value="1">
              <input type="hidden" name="user_access" value="1">

              <div class="form-group">
                <label for="nama">Nama Lengkap</label>
                <input type="text" class="form-control" id="nama" name="nama" required="true" placeholder="masukkan nama">

              </div>
              <div class="form-group">
                <label for="email">Email</label>
                <input type="text" class="form-control" id="email" name="email" required="true">
              </div>
              <div class="form-group">
                <label for="password">Password</label>
                <input type="password" class="form-control" id="password" name="password" required="true">
              </div>
              <button type="submit" name="daftar" class="btn btn-success btn-sm float-right" >Daftar Selesai <i class="fa fa-check"></i></button>
              <p>Sudah punya Akun ?  <a href="" class="text-info" data-dismiss="modal" data-toggle="modal" data-target="#ModalLogin">Login Sekarang</a></p>
            </form>
        </div>
        </div>
      </div>
    </div>
  </div> -->

  <!-- Logout Modal-->
  <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Keluar dari Aplikasi ?</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <!-- <div class="modal-body"></div> -->
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
          <a class="btn btn-primary" href="<?= base_url('member/logout'); ?>">OK</a>
        </div>
      </div>
    </div>
  </div>


