<!-- Default footer-->
      <footer class="section-relative section-top-66 section-bottom-34 page-footer bg-gray-base context-dark novi-background bg-cover text-center">
        <div class="container">
          <div class="row row-fix justify-content-md-center text-xl-left">
            <div class="col-md-8 col-lg-12">
              <div class="row row-fix justify-content-sm-center">
                <div class="col-sm-7 text-sm-left col-lg-4 col-xl-3 order-xl-4">
                  <h6 class="text-uppercase text-spacing-60">Informasi</h6>
                  <div class="widget-post-wrap">
                          <!-- Post Widget-->
                          <article class="post widget-post text-left">
                              <div class="unit flex-row unit-spacing-xs align-items-center">
                                <div class="unit-body">
                                  <div class="post-meta"><span class="icon-xxs text-primary mdi mdi-phone"></span>
                                    <time class="text-dark">Telepon</time>
                                  </div>
                                  <div class="post-title">
                                    <h6 class="text-regular">(031) 8292591</h6>
                                  </div>
                                </div>
                              </div></article>
                          <!-- Post Widget-->
                          <article class="post widget-post text-left"><a href="#">
                              <div class="unit flex-row unit-spacing-xs align-items-center">
                                <div class="unit-body">
                                  <div class="post-meta"><span class="icon-xxs text-primary mdi mdi-map-marker-radius"></span>
                                    <time class="text-dark" datetime="2018-01-01">Alamat Kantor</time>
                                  </div>
                                  <div class="post-title">
                                    <h6 class="text-regular">JL. AHMAD YANI NO. 152C</h6>
                                  </div>
                                </div>
                              </div></a></article>
                          <!-- Post Widget-->
                          <article class="post widget-post text-left">
                              <div class="unit flex-row unit-spacing-xs align-items-center">
                                <div class="unit-body">
                                  <div class="post-meta"><span class="icon-xxs text-primary mdi mdi-map-marker-multiple"></span>
                                    <time class="text-dark">Kota / Provinsi</time>
                                  </div>
                                  <div class="post-title">
                                    <h6 class="text-regular">Surabaya Jawa Timur</h6>
                                  </div>
                                </div>
                              </div></article>
                  </div>
                </div>
                <div class="col-sm-5 offset-top-41 offset-sm-top-0 text-sm-left col-lg-3 col-xl-2 order-xl-3">
                  <h6 class="text-uppercase text-spacing-60">Link</h6>
                  <div class="d-block">
                    <div class="d-inline-block">
                      <ul class="list list-marked">
                        <li><a href="#">Produk Populer</a></li>
                        <li><a href="#">Produk Terbaru</a></li>
                        <li><a href="#">Daftar UKM</a></li>
                      </ul>
                    </div>
                  </div>
                </div>
                <div class="col-sm-12 offset-top-41 col-lg-5 offset-lg-top-0 text-lg-left col-xl-4 order-xl-2">
                  <h6 class="text-uppercase text-spacing-60">Garasi PUEM</h6>
                  <p>Produk Usaha Ekonomi Masyarakat</p>
                  <div class="offset-top-30">
                          <form class="rd-mailform" data-form-output="form-subscribe-footer" data-form-type="subscribe" method="post" action="bat/rd-mailform.php">
                            <div class="form-group">
                              <div class="input-group input-group-sm"><span class="input-group-prepend"><span class="input-group-text input-group-icon"><span class="novi-icon mdi mdi-email"></span></span></span>
                                <div class="form-wrap">
                                  <label class="form-label" for="footer-name">Type your E-Mail</label>
                                  <input class="form-control" id="footer-name" type="email" name="email" data-constraints="@Required @Email">
                                </div><span class="input-group-append">
                                  <button class="btn btn-sm btn-primary" type="submit">Subscribe</button></span>
                              </div>
                            </div>
                            <div class="form-output" id="form-subscribe-footer"></div>
                          </form>
                  </div>
                </div>
                <div class="col-sm-12 offset-top-41 offset-lg-top-66 col-xl-3 order-xl-1 offset-xl-top-0">
                  <!-- Footer brand-->
                  <!-- <div class="footer-brand"><a href="index.html"><img width='250' height='40' src='<?= base_url(); ?>assets/image/logo-provinsi-jawa-timur-white.png' alt=''/></a></div> -->
                        <ul class="list-inline">
                          <li class="list-inline-item"><a class="icon novi-icon fa fa-facebook icon-xxs icon-circle icon-darkest-filled" href="#"></a></li>
                          <li class="list-inline-item"><a class="icon novi-icon fa fa-twitter icon-xxs icon-circle icon-darkest-filled" href="#"></a></li>
                          <li class="list-inline-item"><a class="icon novi-icon fa fa-google-plus icon-xxs icon-circle icon-darkest-filled" href="#"></a></li>
                          <li class="list-inline-item"><a class="icon novi-icon fa fa-instagram icon-xxs icon-circle icon-darkest-filled" href="#"></a></li>
                        </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="container offset-top-50">
          <p class="small text-darker">Garasi PUEM &copy; Dinas Pemberdayaan Masyarakat dan Desa Provinsi Jawa Timur -  <span class="copyright-year"></span>
          </p>
        </div>
      </footer>
    </div>
    <!-- Global Mailform Output-->
    <div class="snackbars" id="form-output-global"></div>
    <!-- Java script-->
    <!-- <script type="text/javascript" src="//code.jquery.com/jquery-1.11.0.min.js"></script>
    <script type="text/javascript" src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script> -->

    <!-- <script src="<?= base_url(); ?>assets/js/jquery-3.5.1.slim.js"></script> -->
    <script src="<?= base_url(); ?>assets/tema/js/core.min.js"></script>
    <!-- <script src="<?= base_url(); ?>assets/js/jquery3.min.js"></script> -->
    <script src="<?= base_url(); ?>assets/tema/slick-1.8.1/slick/slick.min.js"></script>
    <script src="<?= base_url(); ?>assets/tema/js/script.js"></script>
    <script src="<?= base_url(); ?>assets/tema/datepicker/js/bootstrap-datepicker.min.js"></script>
    <script src="<?= base_url(); ?>assets/tema/image-zoom/js/zoom.js"></script>
    <script src="<?= base_url(); ?>assets/js/jquery.autocomplete.min.js"></script>
    <script src="<?= base_url(); ?>assets/js/jquery-ui-1.12.1/jquery-ui.min.js"></script>
    <script src="<?= base_url(); ?>assets/js/tipped/dist/js/tipped.min.js"></script>
    <script src="<?= base_url(); ?>assets/js/custom.js"></script>

    <script type="text/javascript">
      $('#search-produk').autocomplete({
          source: '<?= base_url('home/produk_api') ?>',
          minLength : 1,
          select: function (event, ui) {
            console.log(ui);
            document.location = '<?= base_url('home/detail/') ?>'+ui.item.id;
              //alert('Pilih Produk: ' + ui.item.value + ', ' + ui.item.id);
          }
      });
    </script>
  </body>
</html>
