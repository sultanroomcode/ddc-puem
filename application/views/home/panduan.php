<section class="context-dark">
  <div class="parallax-container" data-parallax-img="<?= base_url(); ?>assets/image/panduan.png">
    <div class="parallax-content">
      <div class="container section-top-34 section-lg-top-98 section-bottom-34 text-center">
        <div>
          <h3 class="font-weight-bold">Panduan Pembelanjaan</h3>
        </div>
        <ul class="list-inline list-inline-dashed p text-light breadcrumb-modern offset-top-10 offset-lg-top-66">
          <li class="list-inline-item active"><a href="<?= base_url(); ?>">Home</a></li>
          <li class="list-inline-item">Panduan Belanja</li>
        </ul>
      </div>
    </div>
  </div>
</section>
<section class="section-66 section-lg-110 novi-background bg-cover">
  <div class="container">
    <div class="row row-fix row-30">
      <div class="col-lg-6">
        <div class="embed-responsive embed-responsive-16by9 shadow">
          <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/4QdRF7p4dHM?rel=0" allowfullscreen></iframe>
        </div>
      </div>
      <div class="col-lg-6 text-lg-left">
        <div class="inset-lg-left-40">
          <h4><b>Bagaimana cara mulai belanja ?</b></h4>
          <p><b>Daftar / Login Sebagai Member</b></p>
          <p>1. Untuk dapat melakukan pembelian anda harus melakukan pendaftaran terlebih dahulu dengan cara Klik tombol <a href="<?= base_url('member/daftar'); ?>"><b>DAFTAR</b></a> dengan mengisi Nama lengkap, Email anda dan password<br>
          2. Aktifasi email yang telah anda daftarkan dengan cara membuka email anda dan klik <i>aktivasi</i><br>
          3. Selanjutnya login dengan cara klik tombol <a href="<?= base_url('member/login'); ?>"><b>LOGIN</b></a> dengan memasukkan email dan password anda<br>
          4. Anda mengisi detail diri anda<br>
          5. Anda dapat memilih produk yang anda inginkan<br>
          6. Selanjutnya klik tombol MASUKKAN KERANJANG<br>
          7. Selanjutnya klik tombol BELI <br>
          8. Untuk proses lebih lanjut anda dapat menghubungi penjual<br>

        </p>

        </div>
      </div>
    </div>
  </div>
</section> 