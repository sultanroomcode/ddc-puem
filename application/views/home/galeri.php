<?php 
$url_base_image = $this->config->item('base_url_image');
?>
<section class="context-dark">
  <div class="parallax-container" data-parallax-img="<?= base_url(); ?>assets/tema/images/bg-header-1920x362.jpg">
    <div class="parallax-content">
      <div class="container section-top-34 section-bottom-34 text-center">
        <div><h3 class="font-weight-bold"><i class="fa fa-image"></i> Galeri</h3></div>
        <ul class="list-inline list-inline-dashed p text-light breadcrumb-modern">
          <li class="list-inline-item active"><a href="<?= base_url();?>">Home</a></li>
          <li class="list-inline-item"> Galeri</li>
        </ul>
      </div>
    </div>
  </div>
</section>

<section class="section-66 section-lg-110 novi-background bg-cover">
  <div class="container">

       <div class="box-galeri">
        <?php foreach ($galeri as $gal) { ?>
          <div class="grid-item">
            <img src="<?= $url_base_image.'userfile/puem/'.$gal['foto'] ?>"
                data-action="zoom" style="cursor: pointer;" class="img-fluid shadow rounded" alt="">
          </div>
        <?php } ?>
       </div>
    </div>
</section>