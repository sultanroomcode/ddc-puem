<?php 
$url_base_image = $this->config->item('base_url_image');
?>
<section class="context-dark">
  <!-- Swiper-->
  <div class="swiper-container swiper-slider" data-height="30%" data-min-height="500px" data-dots="true" data-autoplay="5000">
    <div class="swiper-wrapper">
      <div class="swiper-slide swiper-slide-overlay-disable" data-slide-bg="<?= base_url(); ?>assets/image/slide/slide-1.png" style="background-position: center center"></div>
      <div class="swiper-slide swiper-slide-overlay-disable" data-slide-bg="<?= base_url(); ?>assets/image/slide/slide-2.png" style="background-position: center center"></div>
      <!-- <div class="swiper-slide swiper-slide-overlay-disable" data-slide-bg="<?= base_url(); ?>assets/tema/images/slide-03-1920x640.jpg" style="background-position: center center"></div> -->
    </div>
    <div class="swiper-caption-wraper">
      <div class="container section-41 text-center">
        <h1 class="font-weight-bold mb-minus-10">Garasi PUEM</h1>
        <h5 class="mb-minus-10 font-weight-bold">Gerai Restorasi Pemberdayaan Usaha Ekonomi Masyarakat</h5>
        <h6>Dinas Pemberdayaan Masyarakat & Desa <br><p>Provinsi Jawa Timur</p></h6>
        <div class="offset-top-30">
          <div class="group text-center"><a class="btn btn-success btn-sm" href="<?= base_url('home/about'); ?>">selengkapnya</a><a class="btn btn-default btn-sm" href="">Kontak Kami</a></div>
        </div>
      </div>
    </div>
    <!-- Swiper Pagination-->
    <div class="swiper-pagination swiper-pagination-type-2"></div>
  </div>
</section>
<!--Section thumbnails terry-->
<section class="section-34 novi-background bg-cover">
  <div class="container-fluid">
    <div class="row row-30 justify-content-sm-center">
      <div class="col-sm-6 col-md-3 col-xl-3 col-xs-6">
        <!-- Thumbnail Terry-->
        <figure class="thumbnail-terry"><a href=""><img width="442" height="280" src="<?= base_url(); ?>assets/image/slide/home-img-1.png" alt=""/></a>
          <figcaption>
            <div>
              <h4 class="thumbnail-terry-title">Produk Terpopuler </h4>
            </div>
            <p class="thumbnail-terry-desc offset-top-0">32 produk</p><a class="btn offset-top-10 offset-lg-top-0 btn-primary btn-xs" href="#pupuler">Selengkapnya</a>
          </figcaption>
        </figure>
      </div>
      <div class="col-sm-6 col-md-3 col-xl-3 col-xs-6">
        <!-- Thumbnail Terry-->
        <figure class="thumbnail-terry"><a href="#"><img width="442" height="280" src="<?= base_url(); ?>assets/image/slide/home-img-2.png" alt=""/></a>
          <figcaption>
            <div>
              <h4 class="thumbnail-terry-title">Produk Terbaru</h4>
            </div>
            <p class="thumbnail-terry-desc offset-top-0">54 produk</p><a class="btn offset-top-10 offset-lg-top-0 btn-primary btn-xs" href="#produk">Selengkapnya</a>
          </figcaption>
        </figure>
      </div>
      <div class="col-sm-6 col-md-3 col-xl-3 col-xs-6">
        <!-- Thumbnail Terry-->
        <figure class="thumbnail-terry"><a href="#"><img width="442" height="280" src="<?= base_url(); ?>assets/image/slide/home-img-3.png" alt=""/></a>
          <figcaption>
            <div>
              <h4 class="thumbnail-terry-title">Daftar UKM</h4>
            </div>
            <p class="thumbnail-terry-desc offset-top-0">32 UKM</p><a class="btn offset-top-10 offset-lg-top-0 btn-primary btn-xs" href="<?= base_url(); ?>home/daftarUkm">Selengkapnya</a>
          </figcaption>
        </figure>
      </div>
      <div class="col-sm-6 col-md-3 col-xl-3 col-xs-6">
        <!-- Thumbnail Terry-->
        <figure class="thumbnail-terry"><a href="#"><img width="442" height="280" src="<?= base_url(); ?>assets/image/slide/home-img-4.png" alt=""/></a>
          <figcaption>
            <div>
              <h4 class="thumbnail-terry-title">Informasi Pelatihan</h4>
            </div>
            <p class="thumbnail-terry-desc offset-top-0">3 Informasi</p><a class="btn offset-top-10 offset-lg-top-0 btn-primary btn-xs" href="<?=base_url(); ?>home/infoPelatihan">Selengkapnya</a>
          </figcaption>
        </figure>
      </div>
    </div>
  </div>
</section>


<section class="kategori">
  <h6 class="text-center text-muted">Kategori</h6>
  <div class="container">
    <div class="row slide-kategori">
      <?php foreach ($kategori as $k) { ?>
      <div class="col-sm-6 col-md-3 col-lg-2 text-center">
        <a href="<?= base_url().'kategori/'. $k['slug']; ?>">
          <div class="card">
            <div class="card-body">
              <?= $k['label']; ?>
            </div>
          </div>
        </a>
      </div>
      <?php } ?>

    </div>
  </div>
</section>

<!--Section Produks-->
<br>
<section class="mt-3 novi-background bg-cover" id="produk">
  <div class="container">
    <h3 class="font-weight-bold text-center">Produk</h3>
    <hr class="divider bg-saffron">
    <p class="text-left mb-minus-40">Produk Terbaru <small class="badge badge-primary text-white"> New </small> <small class="float-right text-info"><a href=""> Produk lainnya </a></small></p>
    <div class="row row">
      <?php foreach ($kategori_product as $prod) { ?>
        
      <div class="col-lg-2 col-md-4 col-sm-4 col-xs-6">
        <div class="box-products mb-3 shadow p-all-5">
          <a href="<?= base_url(); ?>home/detail/<?= $prod['idp']; ?>">
          	<div class="img-cover">
	            <img class="img-fluid d-inline-block" width="100%" height="140" src="<?= $url_base_image.'userfile/'.$prod['kd_desa'].'/puem/'.$prod['foto_produk'] ?>" alt="">
	         </div>
          </a>
          <div class="text-sm-left box-products-inner">
            <div class="text-title-produk"><small class="text-info"  >
              <a href="<?= base_url(); ?>home/detail/<?= $prod['idp']; ?>"><b><?= $prod['nama']; ?></b></a>
            </small></div>
            <small class=""><?= $prod['harga']; ?><span class="float-right star mt-2"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i></span></small>
            <ul class="list-inline list-inline-dotted text-dark">
              <li class="list-inline-item"><small style="font-size: 11px;" ><?= $prod['label']; ?></small></li>
              <!-- <li class="list-inline-item"><small><?= $prod['created_by']; ?> <i class="mdi mdi-account"></i></small></li> -->
              <li class="list-inline-item"><small><?= $prod['jml_view']; ?> <i class="fa fa-eye"></i></small></li>
            </ul>
            <!-- <div>
              <p>AVA Nob Hill includes studios and 1 and 2 bedroom apartments that feature an urban-inspired design that extends beyond your walls and throughout the entire community.</p>
            </div> -->
          </div>
        </div>
      </div>
      <?php } ?>

    </div>
    <div class="offset-top-20 text-right"><a class="btn btn-default btn-xs" href=""><small>terbaru lainnya</small></a></div>
    <br>
    <hr>
  </div>
</section>