<section class="context-dark">
  <div class="parallax-container" data-parallax-img="<?= base_url(); ?>assets/tema/images/bg-header-1920x362.jpg">
    <div class="parallax-content">
      <div class="container section-top-34 section-bottom-34 text-center">
        <div><h3 class="font-weight-bold">Daftar UKM</h3></div>
        <ul class="list-inline list-inline-dashed p text-light breadcrumb-modern">
          <li class="list-inline-item active"><a href="<?= base_url();?>">Home</a></li>
          <li class="list-inline-item"> Daftar UKM</li>
        </ul>
      </div>
    </div>
  </div>
</section>


<section class="section-top-34 section-lg-top-66 content">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="card">
					<div class="card-body">							
						<table class="table table-responsive">
							<tr>
								<th>No.</th>
								<th>Nama Pemilik</th>
								<th>Alamat</th>
								<th>Deskripsi</th>
							</tr>

							<?php 
							$i = 1;
							foreach ($member as $m) { ?>
							
							<tr>
								<td><?= $i++ ?></td>
								<td><?= $m['pemilik']; ?></td>
								<td><?= $m['alamat']; ?></td>
								<td><?= $m['deskripsi']; ?></td>
							</tr>
							<?php } ?>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<br><br>