<?php 
$url_base_image = $this->config->item('base_url_image');
?>

<section class="context-dark">
  <div class="parallax-container" data-parallax-img="<?= base_url(); ?>assets/tema/images/bg-header-1920x362.jpg">
    <div class="parallax-content">
      <div class="container section-top-34 section-bottom-34 text-center">
        <div><h3 class="font-weight-bold">Informasi Pelatihan</h3></div>
        <ul class="list-inline list-inline-dashed p text-light breadcrumb-modern">
          <li class="list-inline-item active"><a href="<?= base_url();?>">Home</a></li>
          <li class="list-inline-item"> Informasi Pelatihan</li>
        </ul>
      </div>
    </div>
  </div>
</section>

<section class="section-top-34 section-lg-top-66 content">
	<div class="container">
		<div class="row">
			<?php foreach ($pelatihan as $p) { ?>
			<div class="col-lg-2 col-md-3 col-sm-4 col-6 mb-3">
				<div class="card h-100">
					<div class="card-pelatihan">
						<a href="<?= base_url().'home/detail_pelatihan/'.$p['id'] ?>">
							<img class="img-fluid rounded-top" src="<?= $url_base_image.'userfile/puem/'.$p['cover'] ?>" alt="Cover Informasi">
						</a>
					</div>
					<div class="p-all-5">
						<div class="min-height-50 text-ellipsis mt-2"><p><?= $p['judul']; ?></p></div>
						<small class="text-muted text-10"><?= date('d F Y', strtotime($p['tanggal'])); ?></small>
						<div class="mt-2">
							<a href="<?= base_url().'home/detail_pelatihan/'.$p['id'] ?>" class="btn btn-info btn-xs btn-block">detail</a>
							<!-- <?php if($this->session->userdata('masuk') == TRUE): ?>
								<a href="<?= base_url().'event-register/'.$p['id'] ?>" class="btn btn-info btn-xs btn-block">Daftar Event</a>
							<?php else: ?>
								<small class="text-danger"><i class="fa fa-warning"></i> Login Terlebih Dahulu</small>
							<?php endif; ?> -->
						</div>
					</div>
				</div>
			</div>
			<?php } ?>
		</div>
	</div>
</section>
<br><br><br>
<!-- <section class="section-top-34 section-lg-top-66 content">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="card">
					<div class="card-body">							
						<table class="table table-responsive">
							<tr>
								<th>No.</th>
								<th>Cover</th>
								<th>Judul</th>
								<th>Deskripsi</th>
								<th>Tanggal Kegiatan</th>
								<th>Di unggah pada</th>
								<th>Aksi</th>
							</tr>

							<?php 
							$i = 1;
							foreach ($pelatihan as $p) { ?>
							
							<tr>
								<td><?= $i++ ?></td>
								<td>
									<a href="#zoom-image/<?= $p['id']; ?>">
										<img class="img-fluid" src="<?= $url_base_image.'userfile/puem/'.$p['cover'] ?>" alt="Cover Informasi" style="width:100px">
									</a>
									<div class="pop-overlay" id="zoom-image/<?= $p['id']; ?>">
										<a href="#" class="close"> x close</a>
										<img class="img-fluid" src="<?= $url_base_image.'userfile/puem/'.$p['cover'] ?>" alt="Cover Informasi">
									</div>
								</td>
								<td><?= $p['judul']; ?></td>
								<td><?= $p['deskripsi']; ?></td>
								<td><?= $p['tanggal']; ?></td>
								<td><?= $p['created_at']; ?></td>
								<td>
									<?php if($this->session->userdata('masuk') == TRUE): ?>
										<a href="<?= base_url().'event-register/'.$p['id'] ?>" class="btn btn-info btn-xs btn-block">Daftar Event</a>
									<?php else: ?>
										<small class="text-danger"><i class="fa fa-warning"></i> Login Terlebih Dahulu</small>
									<?php endif; ?>	
								</td>
							</tr>
							<?php } ?>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<br><br> -->