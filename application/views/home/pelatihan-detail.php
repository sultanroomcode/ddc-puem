<?php 
$url_base_image = $this->config->item('base_url_image');
?>
<section class="context-dark">
  <div class="parallax-container" data-parallax-img="<?= base_url(); ?>assets/tema/images/bg-header-1920x362.jpg">
    <div class="parallax-content">
      <div class="container section-top-34 section-bottom-34 text-center">
        <div><h3 class="font-weight-bold"><i class="fa fa-folder"></i> Detail Pelatihan</h3></div>
        <ul class="list-inline list-inline-dashed p text-light breadcrumb-modern">
          <li class="list-inline-item active"><a href="<?= base_url();?>">Home</a></li>
          <li class="list-inline-item active"><a href="<?= base_url('pelatihan');?>">Pelatihan</a></li>
          <li class="list-inline-item">Detail Pelatihan</li>
        </ul>
      </div>
    </div>
  </div>
</section>

<section class="section-top-34 section-lg-top-66 content">
	<div class="container">
		<div class="row">
				<div class="col-md-8">
            <div class="card">
              <div class="card-body">
                <div class="text-center">
                  <h4 class="my-3"><b><?= $pelatihan['judul'] ?></b></h4><br>
                  <img src="<?= $url_base_image.'userfile/puem/'.$pelatihan['cover'] ?>" class="img-fluid" style="width:90%"><br>
                </div><br>
                <h5><b>Deskripsi</b></h5>
								<p><?= $pelatihan['deskripsi'] ?></p>
								<p>Tanggal : <?= $pelatihan['tanggal'] ?></p><br>
					    </div>
								
							</div>
					</div>
          <div class="col-md-4">
            <div class="card">
              <div class="card-body">
                  <h5 class="text-center mt-3"><b>Daftar Peserta </b><br></h5>

                  <!-- data peserta sesuai acara -->
                  <div class="text-muted text-center mb-2"><p>Apakah Anda ingin mengikuti acara ini ?</p></div>
                    <?php if($this->session->userdata('masuk') == TRUE): ?>
                      <a href="<?= base_url().'event-register/'.$pelatihan['id'] ?>" class="btn btn-info btn-sm btn-block">Ikuti acara</a>
                    <?php else: ?>
                      <a href="<?= base_url('member/login'); ?>" class="btn btn-info btn-sm btn-block"> ikuti acara</a>
                    <?php endif; ?> 

                  <hr>
                  <div class="text-muted text-center mb-2"><p>Data Peserta yang sudah mendaftar</p></div>
                  <?php  if(count($peserta) > 0) { 
                      foreach ($peserta as $v) { ?>
                        <i class="fa fa-check text-success"></i> <?= $v['nama'] ?><br>
                      <?php }
                    } else { ?>
                      <div class="text-center"><p>Belum ada Peserta</p></div>
                  <?php } ?>

                  <hr>
                 
                 
              </div>
            </div>
          </div>
		</div>
	</div>
</section>
<br><br>