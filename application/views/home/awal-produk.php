<?php 
$url_base_image = $this->config->item('base_url_image');
?>
<!-- produk terbaru -->
<br>
<p class="text-left mb-minus-40">Produk Terbaru <small class="badge badge-primary text-white"> New </small> <small class="float-right text-info"><a href=""> Produk lainnya </a></small></p>
<br>
<div class="row">
  <?php foreach ($produk as $prod) { ?>
  <div class="col-lg-2 col-md-3 col-sm-3 col-6 mb-3">
    <div class="box-products border rounded h-100">
      <a href="<?= base_url(); ?>home/detail/<?= $prod['idp']; ?>">
      	<div class="img-cover">
          <img class="img-fluids rounded-top d-inline-block" width="100%"  src="<?= $url_base_image.'userfile/'.$prod['kd_desa'].'/puem/'.$prod['foto_produk'] ?>" alt="">
       </div>
      </a>
      <div class="text-sm-left box-products-inner p-all-5">
        <div class="text-title-produk"><small class="text-info">
          <a href="<?= base_url(); ?>home/detail/<?= $prod['idp']; ?>"><b><?= $prod['nama']; ?></b></a>
        </small></div>
        <small class="">Rp <?= number_format((int) $prod['harga'], 2, ',','.') ?><span class="float-right star mt-2"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i></span></small>
        <ul class="list-inline text-dark">
          <?php foreach($this->Home_model->getAllKategoriByProduk($prod['idp']) as $data){ ?>
            <li class="list-inline-item kategori"><small style="font-size: 11px;"><?= $data['label']; ?></small></li><br>
          <?php } ?>
          <li class="list-inline-item">
            <small style="font-size: 11px;">
              <?= $prod['jml_view']; ?> <i class="fa fa-eye"></i>
            </small> 
          </li>
          <li class="list-inline-item">
            <small style="font-size: 11px;">

            <?php $buy = $this->Member_model->getJumlahBuy($prod['idp']); echo ($buy != null)?$buy['total']:0;
            ?>

           <i class="fa fa-shopping-cart"></i></small>
         </li>
        </ul>
      </div>
    </div>
  </div>
  <?php } ?>
</div>
<div class="offset-top-20 text-right">
  <a class="btn btn-default btn-xs" href=""><small>terbaru lainnya</small></a>
</div>
<br><hr> 
<!-- akhir produk terbaru -->

<!-- produk terpopuler -->
<p class="text-left mb-minus-40 mt-3">Produk Terpopuler <small class="badge badge-success text-white star"> <i class="fa fa-star"></i> <i class="fa fa-star"></i> </small><small class="float-right text-success"><a href=""> Produk lainnya </a></small></p><br>
<div class="row">
<?php foreach ($produkPop as $prod) { ?>
  <div class="col-lg-2 col-md-3 col-sm-3 col-6 mb-3">
    <div class="box-products border rounded h-100">
      <a href="<?= base_url(); ?>home/detail/<?= $prod['idp']; ?>">
        <div class="img-cover">
          <img class="img-fluid rounded-top d-inline-block" width="100%" height="140" src="<?= $url_base_image.'userfile/'.$prod['kd_desa'].'/puem/'.$prod['foto_produk'] ?>" alt="">
      </div>
      </a>
      <div class="text-sm-left box-products-inner p-all-5">
        <div class="text-title-produk"><small class="text-success">
          <a href="<?= base_url(); ?>home/detail/<?= $prod['idp']; ?>"><b><?= $prod['nama']; ?></b></a>
        </small></div>
        <small class="">Rp <?= number_format((int) $prod['harga'], 2, ',','.') ?><span class="float-right star mt-2"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i></span></small>
        <ul class="list-inline text-dark">
          <?php foreach($this->Home_model->getAllKategoriByProduk($prod['idp']) as $data){ ?>
            <li class="list-inline-item kategori"><small style="font-size: 11px;"><?= $data['label']; ?></small></li><br>
          <?php } ?>
          <li class="list-inline-item">
            <small style="font-size: 11px;">
              <?= $prod['jml_view']; ?> <i class="fa fa-eye"></i>
            </small> 
          </li>
          <li class="list-inline-item">
            <small style="font-size: 11px;">

            <?php $buy = $this->Member_model->getJumlahBuy($prod['idp']); echo ($buy != null)?$buy['total']:0;
            ?>

           <i class="fa fa-shopping-cart"></i></small>
         </li>
        </ul>
      </div>
    </div>
  </div>
  <?php } ?>
  </div>
  <div class="offset-top-20 text-right">
    <a class="btn btn-default btn-xs" href=""><small>terpopuler lainnya</small></a>
  </div>
  <br><hr> <!-- akhir terpopuler -->