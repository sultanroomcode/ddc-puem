<?php 
//$base_url_image = $this->config->item('base_url_file_storage');
$base_url_image = $this->config->item('base_url_image');
?>
<section class="context-dark text-center">
  <div class="parallax-container" data-parallax-img="<?= base_url(); ?>assets/tema/images/bg-header-1920x362.jpg">
    <div class="parallax-content min-hight-300">
      <div class="container section-top-34 section-md-top-98 section-bottom-34">
        <div>
          <h1 class="font-weight-bold"><?=$member['pemilik'];?></h1>
        </div>
        <ul class="list-inline list-inline-dashed p text-light breadcrumb-modern offset-top-10 offset-lg-top-66">
          <li class="list-inline-item text-info active"><a href="<?= base_url(); ?>">Home</a></li>
          <li class="list-inline-item text-info"><a href="#">Member</a></li>
          <li class="list-inline-item"> <?=$member['pemilik'];?></li>
        </ul>
      </div>
    </div>
  </div>
</section>

<section class="section-66 section-lg-110 bg-lighter novi-background bg-cover">
  <div class="container text-center">
    <h3 class="font-weight-bold">Tentang <span class="text-success"><?=$member['pemilik'];?></span></h3>
    <hr class="divider bg-saffron">
    <div class="offset-top-41 offset-lg-top-66">
      <div class="row row-fix row-50">
        <div class="col-md-4 shadow"><img class="img-fluid d-inline-block pt-3" src="<?= base_url(); ?>assets/image/user.jpeg" width="370" height="370" alt=""><a class="btn btn-block btn-primary offset-top-34 d-inline-block" href="#" style="max-width: 370px"><?=$member['pemilik'];?></a>
          <div class="mt-3">
            <address class="contact-info text-lg-center">
              <ul class="list-unstyled p">
                <?php foreach ($contact as $ct ) { ?>
                  <a href="<?=$ct['text_contact'];?>" target="_blank" ><span class="novi-icon icon icon-xxs mdi mdi-<?=$ct['type_contact']?> text-middle px-2 text-info"></span></a>                 
                <?php } ?>
               
              </ul>
            </address>
            <br>
          </div>
        </div>
        <div class="col-md-8">
          <div class="inset-lg-left-50">
            <div class="text-left">
              <p><?= $member['alamat']; ?></p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
 <section class="section-66 section-lg-110 novi-background bg-cover">
  <div class="container text-center">
    <h3 class="font-weight-bold">Produk <span class="text-success"> <?=$member['pemilik'];?></span></h3>
    <hr class="divider bg-saffron">
    <div class="offset-top-41 offset-lg-top-66">
      <div class="row row-60">
        
       <?php foreach ($prodmember as $prods) { ?>
       <div class="col-sm-6 col-md-4 col-lg-3">
        <div class="box-products mb-3 shadow p-all-5">
          <!-- <a href="" data-toggle="modal" data-target="#detailProdukMember">
            <img class="img-fluid d-inline-block" src="<?= base_url(); ?>assets/tema/images/home-img-05-370x250.jpg" width="370" height="250" alt="">
          </a> -->
           <a href="<?= base_url(); ?>home/detail/<?= $prods['idp']; ?>" data-toggle="modal" data-target="#detailProdukMember">
            <img class="img-fluid d-inline-block" src="<?= $base_url_image.'userfile/'.$prods['kd_desa'].'/puem/'.$prods['foto_produk'] ?>" width="370" height="250" alt="">
          </a>
          <div class="text-md-left box-products-inner">
            <p class="text-info">
              <a href="<?= base_url(); ?>home/detail/<?= $prods['idp']; ?>" onclick="changeData({kd_desa:'<?=$prods['kd_desa']?>', idp:'<?=$prods['idp']?>', judul:'<?=$prods['nama']?>', deskripsi:'<?=$prods['deskripsi']?>', harga:'<?=$prods['harga']?>'})" data-toggle="modal" data-target="#detailProdukMember"><b><?= $prods['nama']; ?></b></a>
            </p>
            <small class="text-bold">Rp <?= $prods['harga']; ?></small>
            <ul class="list-inline list-inline-dotted text-dark">
              <li class="list-inline-item"><small>kategori</small></li>
              <li class="list-inline-item"><small><?= $prods['created_by']; ?> <i class="mdi mdi-account"></i></small></li>
              <li class="list-inline-item"><small><?= $prods['jml_view']; ?> <i class="fa fa-eye"></i></small></li>
            </ul>
          </div>
        </div>
      </div>
      <?php } ?>
       

      </div>
      <div class="offset-top-41 offset-lg-top-66 text-center">
        <!-- Classic Pagination-->
        <nav>
          <ul class="pagination-classic">
            <li><a href="#">Prev</a></li>
            <li class="active"><a href="#">1</a></li>
            <li><a href="#">2</a></li>
            <li><a href="#">3</a></li>
            <li><a href="#">4</a></li>
            <li><a href="#">Next</a></li>
          </ul>
        </nav>
      </div>
    </div>
  </div>
</section>



<!-- Wait Modal-->
<div class="modal fade" id="detailProdukMember" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <br>
      <div class="modal-content">
        
        <div class="modal-body"><p class="mt-3 ml-3">Produk <span class="text-success"><?=$member['pemilik'];?></span></p>
          <hr>
          <section class="novi-background bg-cover bg-default">
            <div class="container">
              <h4 class="font-weight-bold text-center" id="modal-judul">xxx</h4>
              <hr class="divider bg-saffron">
              <div class="">
                <div class="row">
                  <div class="col-md-12">
                    <!-- Owl Carousel-->
                    <div id="modal-galeri"></div>
                    <div class="text-md-left offset-top-30 offset-lg-top-50">
                      <h5 class="font-weight-bold">Deskripsi Produk</h5>
                      <p id="modal-deskripsi"></p>
                    </div>
                    <div class="offset-top-30">
                      <!-- Bootstrap Table-->
                      <div class="table-responsive clearfix">
                        <table class="table table-striped">
                          <tr>
                            <th>Harga</th>
                            <th id="modal-harga"></th>
                          </tr>
                          <tr>
                            <th>Features</th>
                            <td>Lawn, Sprinkler System, Marble Floors</td>
                          </tr>
                          <tr>
                            <th>MLS Listing ID</th>
                            <td>12345678</td>
                          </tr>
                          <tr>
                            <th>Year Built</th>
                            <td>1868</td>
                          </tr>
                          <tr>
                            <th>Lot Size</th>
                            <td>5.45</td>
                          </tr>
                          <tr>
                            <th>Parking Type</th>
                            <td>Garage - Attached</td>
                          </tr>
                          <tr>
                            <th>Room Count</th>
                            <td>6</td>
                          </tr>
                          <tr>
                            <th>Roof Type</th>
                            <td>Tile</td>
                          </tr>
                          <tr>
                            <th>View Type</th>
                            <td>Mountain</td>
                          </tr>
                          <tr>
                            <th>Exterior Type</th>
                            <td>Stucco</td>
                          </tr>
                        </table>
                      </div>
                    </div>
                  </div>
                  
                </div>
              </div>
            </div>
          </section>

        </div>
        <div class="modal-footer">
          <button class="btn btn-info btn-sm" type="button" data-dismiss="modal">ok</button>

        </div>
      </div>
    </div>
</div>

<script type="text/javascript">
function changeData(o)
{
  $('#modal-idp').html(o.idp);
  $('#modal-judul').html(o.judul);
  $('#modal-deskripsi').html(o.deskripsi);
  $('#modal-harga').html(o.harga);

  //send to server : url home/detail/{idp} to update count
  $.ajax({url: "<?=base_url()?>home/detail/"+o.idp, success: function(result){
    // alert(result);
  }});

  //get json picture from server 

  $.getJSON("<?=base_url()?>home/getpicture/"+o.idp+'/'+o.kd_desa, function(result){

    console.log(result);
    txt = "<div class=\"owl-carousel owl-carousel-classic\" data-items=\"1\" data-dots=\"true\" data-nav=\"true\" data-nav-class=\"[&quot;owl-prev mdi mdi-chevron-left&quot;, &quot;owl-next mdi mdi-chevron-right&quot;]\">";
    $.each(result, function(i, field){
      // $("div").append(field + " ");
      txt += "<a class=\"thumbnail-classic\" data-lightgallery=\"item\" href=\""+field+"\"><figure><img src=\""+field+"\" alt=\"\"/></figure></a>";
    });

    txt +="</div>";

    console.log(txt);

    $('#modal-galeri').html(txt);
  });
}
</script>