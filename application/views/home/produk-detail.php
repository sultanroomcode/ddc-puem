<?php 
$url_base_image = $this->config->item('base_url_image');
?>
<section class="context-dark">
  <div class="parallax-container" data-parallax-img="<?= base_url(); ?>assets/tema/images/bg-header-1920x362.jpg">
    <div class="parallax-content">
      <div class="container section-top-34 section-lg-top-98 section-bottom-34 text-center">
        <div>
         <!--  <h1 class="font-weight-bold"><?=$produk['created_by'];?></h1> -->
        </div>
        <ul class="list-inline list-inline-dashed p text-light breadcrumb-modern offset-top-10 offset-lg-top-66">
          <li class="list-inline-item active"><a href="<?= base_url();?>">Home</a></li>
          <li class="list-inline-item"><a href="<?= base_url();?>#produk">Produk</a></li>
          <li class="list-inline-item"><?=$produk['nama'];?></li>
        </ul>
      </div>
    </div>
  </div>
</section>

<!-- Page Contents-->
<!--Section The Presidio Residences-->
<br>

<div class="container">
  <div class="row">
    <div class="col-md-3">
      <div class="card mb-4">
        <div class="card-body">
          <div class="row">
            <div class="col-md-4 col-sm-3 col-4">
              <img class="img-fluid d-inline-block" src="<?= base_url(); ?>assets/image/user.jpeg" alt="">
            </div>
            <div class="col-md-8 col-sm-9 col-8">
              <a href="<?= base_url().'/'.$produk['kd_desa'].'/'.$produk['kd_toko']; ?>">
                <h6 class="text-info font-weight-bold"><?=$produk['pemilik'];?></h6>
              </a>
              <span><?=$produk['alamat'];?></span>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-md-4">
      <div class="shadow owl-carousel owl-carousel-classic" data-items="1" data-dots="true" data-nav="true" data-nav-class="[&quot;owl-prev mdi mdi-chevron-left&quot;, &quot;owl-next mdi mdi-chevron-right&quot;]">

            <a class="thumbnail-classic" data-lightgallery="item" href="<?= $url_base_image.'userfile/'.$produk['kd_desa'].'/puem/'.$produk['foto_produk']; ?>">
              <figure><img src="<?= $url_base_image.'userfile/'.$produk['kd_desa'].'/puem/'.$produk['foto_produk']; ?>" alt=""/>
              </figure>
            </a>

            <?php foreach($produk_photo as $v): ?>
            <a class="thumbnail-classic" data-lightgallery="item" href="<?= $url_base_image.'userfile/'.$produk['kd_desa'].'/puem/'.$v['src'] ?>">
              <figure><img src="<?= $url_base_image.'userfile/'.$produk['kd_desa'].'/puem/'.$v['src'] ?>" alt=""/>
              </figure>
            </a>            
            <?php endforeach; ?>
          </div>
    </div>
    <div class="col-md-5">
      <h4 class="mt-3"><b><?= strtoupper($produk['nama']);?></b></h4><hr>
      <div class="row">
        <div class="col-md-12">
          <div class="d-flex justify-content-start mx-2">
            <small class="text-10 text-muted"><i class="fa fa-eye"></i>
              <b> <?= $produk['jml_view']?></b> x
            </small>
            <small class="text-10 text-muted mx-4"><i class="fa fa-shopping-cart"></i>  
              <?php $buy = $this->Member_model->getJumlahBuy($produk['idp']); echo ($buy != null)?$buy['total']:0;
              ?> Terjual
            </small>
            <small>
              <span class="star">
                <i class="fa fa-star-half-o"></i> 
                <i class="fa fa-star"></i> 
                <i class="fa fa-star"></i> 
                <i class="fa fa-star"></i>
              </span>
            </small>
          </div>
        </div>
      </div>
      <h5 class="text-info"><b>Rp <?= number_format((int) $produk['harga'], 2, ',','.') ?></b></h5>
      <?php if($this->session->ses_id != null) { ?>
        <div class=""><a class="btn btn-danger btn-sm" href="<?= base_url('member/keranjang/'.$produk['idp']); ?>"><i class="fa fa-cart-plus"></i> Masukkan Keranjang</a>
        </div>
      <?php } else { ?>
         <div class=""><a class="btn btn-danger btn-sm" href="" onclick="return confirm('Login Terlebih Dahulu'); "><i class="fa fa-cart-plus"></i> Masukkan Keranjang</a>
        </div>
      <?php } ?>
    </div>
  </div>
</div>
<br>
<div class="container mt-3 ">
<div class="row">
  <div class="offset-md-3 col-md-9">
    <div class="card">
      <div class="card-body">
          <h6 class="mt-3">Deskripsi Produk</h6>
            <div class="row">
              <div class="col-md-3 col-sm-4 col-4">
                <!-- <p class="text-muted">Deskripsi</p> -->
                <p class="text-muted">Ukuran</p>
                <p class="text-muted">Bahan Baku</p>
                <p class="text-muted">Kapasitas Produksi</p>
              </div>
              <div class="col-md-9 col-sm-8 col-8">
                <!-- <p><?=$produk['deskripsi'];?></p> -->
                <p>: <?=$produk['ukuran'];?></p>
                <p>: <?=$produk['bahan_baku'];?></p>
                <p>: <?=$produk['kapasitas_produksi'];?></p>
              </div>
            </div>
        </div>
      </div>
    </div>
  </div>
</div>
<br><br>