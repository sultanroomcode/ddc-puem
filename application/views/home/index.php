<?php 
$url_base_image = $this->config->item('base_url_image');
?>
<section class="context-dark">
  <!-- Swiper-->
  <div class="swiper-container swiper-slider" data-height="30%" data-min-height="500px" data-dots="true" data-autoplay="5000">
    <div class="swiper-wrapper">
      <div class="swiper-slide swiper-slide-overlay-disable" data-slide-bg="<?= base_url(); ?>assets/image/slide/slide-1.png" style="background-position: center center"></div>
      <div class="swiper-slide swiper-slide-overlay-disable" data-slide-bg="<?= base_url(); ?>assets/image/slide/slide-2.png" style="background-position: center center"></div>
      <!-- <div class="swiper-slide swiper-slide-overlay-disable" data-slide-bg="<?= base_url(); ?>assets/tema/images/slide-03-1920x640.jpg" style="background-position: center center"></div> -->
    </div>
    <div class="swiper-caption-wraper">
      <div class="container section-41 text-center">
        <h1 class="font-weight-bold mb-minus-10">Garasi PUEM</h1>
        <h5 class="mb-minus-10 font-weight-bold">Gerai Restorasi Pemberdayaan Usaha Ekonomi Masyarakat</h5>
        <h6>Dinas Pemberdayaan Masyarakat & Desa <br><p>Provinsi Jawa Timur</p></h6>
        <div class="offset-top-30">
          <div class="group text-center"><a class="btn btn-success btn-sm" href="<?= base_url('home/about'); ?>">selengkapnya</a><a class="btn btn-default btn-sm" href="">Kontak Kami</a></div>
        </div>
      </div>
    </div>
    <!-- Swiper Pagination-->
    <div class="swiper-pagination swiper-pagination-type-2"></div>
  </div>
</section>

<div class="flash-data" data-flashdata="<?= $this->session->flashdata('flash'); ?>"></div>
<?php if ( $this->session->flashdata('flash') ) : ?>
  <div class="row mt-3">
    <div class="col-md-6">
      <div class="alert alert-success alert-dismissible fade show" role="alert">Data <strong>berhasil </strong><?= $this->session->flashdata('flash'); ?>
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
      </div>
    </div>
  </div> 
  <?php endif; ?>

<!--Section thumbnails terry-->
<section class="section-34 novi-background bg-cover">
  <div class="container-fluid">
    <div class="row row-30 justify-content-sm-center">
      <div class="col-sm-6 col-md-3 col-xl-3 col-xs-6">
        <!-- Thumbnail Terry-->
        <figure class="thumbnail-terry"><a href=""><img width="442" height="280" src="<?= base_url(); ?>assets/image/slide/home-img-1.png" alt=""/></a>
          <figcaption>
            <div>
              <h4 class="thumbnail-terry-title">Produk Terpopuler </h4>
            </div>
            <p class="thumbnail-terry-desc offset-top-0">32 produk</p><a class="btn offset-top-10 offset-lg-top-0 btn-primary btn-xs" href="#produk">Selengkapnya</a>
          </figcaption>
        </figure>
      </div>
      <div class="col-sm-6 col-md-3 col-xl-3 col-xs-6">
        <!-- Thumbnail Terry-->
        <figure class="thumbnail-terry"><a href="#"><img width="442" height="280" src="<?= base_url(); ?>assets/image/slide/home-img-2.png" alt=""/></a>
          <figcaption>
            <div>
              <h4 class="thumbnail-terry-title">Produk Terbaru</h4>
            </div>
            <p class="thumbnail-terry-desc offset-top-0">54 produk</p><a class="btn offset-top-10 offset-lg-top-0 btn-primary btn-xs" href="#produk">Selengkapnya</a>
          </figcaption>
        </figure>
      </div>
      <div class="col-sm-6 col-md-3 col-xl-3 col-xs-6">
        <!-- Thumbnail Terry-->
        <figure class="thumbnail-terry"><a href="#"><img width="442" height="280" src="<?= base_url(); ?>assets/image/slide/home-img-3.png" alt=""/></a>
          <figcaption>
            <div>
              <h4 class="thumbnail-terry-title">Daftar UKM</h4>
            </div>
            <p class="thumbnail-terry-desc offset-top-0">32 UKM</p><a class="btn offset-top-10 offset-lg-top-0 btn-primary btn-xs" href="<?= base_url(); ?>home/daftar_ukm">Selengkapnya</a>
          </figcaption>
        </figure>
      </div>
      <div class="col-sm-6 col-md-3 col-xl-3 col-xs-6">
        <!-- Thumbnail Terry-->
        <figure class="thumbnail-terry"><a href="#"><img width="442" height="280" src="<?= base_url(); ?>assets/image/slide/home-img-4.png" alt=""/></a>
          <figcaption>
            <div>
              <h4 class="thumbnail-terry-title">Informasi Pelatihan</h4>
            </div>
            <p class="thumbnail-terry-desc offset-top-0">3 Informasi</p><a class="btn offset-top-10 offset-lg-top-0 btn-primary btn-xs" href="<?=base_url(); ?>pelatihan">Selengkapnya</a>
          </figcaption>
        </figure>
      </div>
    </div>
  </div>
</section>


<section class="kategori">
  <h6 class="text-center text-muted">Kategori</h6>
  <div class="container">
    <div class="row slide-kategori">
      <?php foreach ($kategori as $k) { ?>
      <div class="col-sm-6 col-md-3 col-lg-2 text-center">
        <a href="<?= base_url().'kategori/'. $k['slug']; ?>#produk">
          <div class="card">
            <div class="card-body">
              <?= $k['label']; ?>
            </div>
          </div>
        </a>
      </div>
      <?php } ?>

    </div>
  </div>
</section>

<!--Section Produks-->
<br>
<section class="mt-3 novi-background bg-cover" id="produk">
  <div class="containerx padding-produk mb-3">
    <div class="row">
      <div class="col-md-9">
        <div class="row">
          <div class="col-md-12">
            <h3 class="font-weight-bold text-center">Produk</h3>
            <hr class="divider bg-saffron">
            <br>
          </div>
          <div class="col-md-12 mt-2 ml-3">
            <form>
              <div class="row">
                <div class="col-md-3">
                  <div class="form-group mb-1">
                      <select name="kota" id='kota_selects' onchange="filterKecamatan(this.value)">
                        <option>-- Pilih Kabupaten --</option>
                        <?php
                           foreach($kabupaten as $kab){
                             echo "<option value='".$kab['id']."'>".$kab['description']."</option>";
                           }
                         ?>
                      </select>
                  </div>
                </div>  
                <div class="col-md-3">
                  <div class="form-group mb-1">
                    <select name="kecamatan" id='kecamatan_selects' onchange="filterDesa(this.value)">
                      <option>-- Pilih Kecamatan --</option>
                    </select>
                  </div>
                </div>
                <div class="col-md-3">
                  <div class="form-group mb-1">
                      <select name="kelurahan" id='kelurahan_selects' onchange="filterProduk(this.value)">
                        <option>-- Pilih Desa --</option>
                      </select>
                    </div>
                </div> 
              </div>
            </form>
          </div>
        </div>
        

        <div id="produk-by-desa"></div>
        
      </div><br> <!-- akhir col-md-9 -->

      <!-- informasi  -->
      <?php 
        $url_base_image = $this->config->item('base_url_image');
      ?>
      <div class="col-md-3 py-3 mb-3 px-1">
        <h5 class="text-center"><b>Informasi Pelatihan</b></h5>
        <?php foreach ($pelatihan as $p) { ?> 
        <a href="<?= base_url('home/detail_pelatihan/'.$p['id']); ?>">
          <div class="row border border-info rounded my-1 mx-1 py-2">
            <div class="col-md-5 col-sm-4">
              <img class="img-fluid img-rounded" src="<?= $url_base_image.'userfile/puem/'.$p['cover'] ?>" alt="Cover Informasi" style="width:100%">
            </div>
            <div class="col-md-7 col-sm-8"><small><?= $p['judul']; ?></small><br>
              <small class="text-muted"><?= date('d F Y', strtotime($p['tanggal'])); ?></small>
            </div>
          </div>
        </a>
        <?php } ?>
        <div class="offset-top-20 text-center">
          <a class="btn btn-default btn-xs" href="<?= base_url('pelatihan'); ?>"><small>informasi lainnya</small></a>
        </div>
          
      </div>
      <!-- akhir informasi -->

    </div> <!-- akhir row -->
  </div> <!-- akhir container -->
</section>

<script type="text/javascript" src="<?= base_url(); ?>assets/js/jquery.min.js"></script>
<script src="<?= base_url(); ?>assets/js/sweetalert2/sweetalert2.all.min.js"></script>
<script type="text/javascript">
  function filterKecamatan(id_kota)
  {
    $('#kecamatan_selects').load('<?= base_url() ?>wilayah/ddc_filter_kecamatan/'+id_kota);
  }

  function filterDesa(id_kecamatan)
  {
    $('#kelurahan_selects').load('<?= base_url() ?>wilayah/ddc_filter_kelurahan/'+id_kecamatan);
  }

  function filterProduk(input)
  {
    $('#produk-by-desa').load('<?= base_url() ?>home/filter_produk/'+input);
    // $('#produk-by-desa').load('<?= base_url() ?>home/filter_produk/'+input.value+'/'+input.options[input.selectedIndex].text);
  }

  window.onload = function() {
   // /filterKabupaten(15);
   $('#produk-by-desa').load('<?= base_url() ?>home/awal_produk/');
  }
</script>

<!-- modal pop up wellcome-->
<!-- <div class="modal fade mt-3" tabindex="-1" role="dialog" id="mymodalpop-up">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title"></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body text-center">
        <img src="<?= base_url().'image/session.jpg' ?>" class="img-fluid"><br><br>
        <a href="https://bit.ly/3hseuET" class="btn btn-danger btn-block">Klik Disini Untuk Daftar</a>
      </div>
      <div class="modal-footer">
      </div>
    </div>
  </div>
</div> -->




