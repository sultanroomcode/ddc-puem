<section class="context-dark">
  <div class="parallax-container" data-parallax-img="<?= base_url(); ?>assets/tema/images/bg-header-1920x362.jpg">
    <div class="parallax-content">
      <div class="container section-top-34 section-bottom-34 text-center">
        <div><h3 class="font-weight-bold"><i class="fa fa-camera"></i> Video</h3></div>
        <ul class="list-inline list-inline-dashed p text-light breadcrumb-modern">
          <li class="list-inline-item active"><a href="<?= base_url();?>">Home</a></li>
          <li class="list-inline-item"> Video</li>
        </ul>
      </div>
    </div>
  </div>
</section>
<section class="section-66 section-lg-110 novi-background bg-cover">
  <div class="container">
    <div class="row border shadow pt-4">
      <?php foreach ($video as $p) { ?>  
      <div class="col-md-4 mb-4 text-center">
        <p class="mt-2"><?= $p['description'] ?></p>
        <div class="embed-responsive embed-responsive-16by9">
          <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/<?= $p['link_embed'] ?>?rel=0" allowfullscreen></iframe>
        </div> <br> 
      </div>
      <?php } ?>  
    </div>
  </div>
</section>