<section class="context-dark">
  <div class="parallax-container" data-parallax-img="<?= base_url(); ?>assets/tema/images/bg-header-1920x362.jpg">
    <div class="parallax-content">
      <div class="container section-top-34 section-lg-top-98 section-bottom-34 text-center">
        <div>
          <h3 class="font-weight-bold">Tentang Garasi PUEM</h3>
        </div>
        <ul class="list-inline list-inline-dashed p text-light breadcrumb-modern offset-top-10 offset-lg-top-66">
          <li class="list-inline-item active"><a href="<?= base_url(); ?>">Home</a></li>
          <li class="list-inline-item">Tentang PUEM</li>
        </ul>
      </div>
    </div>
  </div>
</section>
<section class="section-66 section-lg-110 novi-background bg-cover">
  <div class="container">
    <div class="row row-fix row-30">
      <div class="col-lg-6">
        <div class="embed-responsive embed-responsive-16by9 shadow">
          <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/2kV0vQZdqv0?rel=0" allowfullscreen></iframe>
        </div>
      </div>
      <div class="col-lg-6 text-lg-left">
        <div class="inset-lg-left-40">
          <p><b>Garasi PUEM</b> adalah Gerai Restorasi Pemberdayaan Usaha Ekonomi Masyarakat</p>
          <p>Merupakan Website untuk mewadahi produk kelompok usaha ekonomi masyarakat, tujuan dari Garasi PUEM adalah sebagai Sarana pengembangan usaha ekonomi, sarana meningkatkan kualitas produk dan Sarana pemasaran produk online.</p><br>
          <p><b>Visi Garasi PUEM</b></p>
          <p>1. Untuk Restorasi Desa ( mendorong semangat gotong royong melalui pemberdayaan Pemdes dan BUM Desa serta insentif inovasi desa )<br>
          2. Suplay dan demand chanel ( penataan pasar tradisional, inklusi UMKM retail modern )<br>
          3. Mendorong Communal Branding ( untuk UMKM, menumbuhkembangkan koperasi Wanita, petani, nelayan dan perdagangan antar pulau )<br>
          4. Menyiapkan wirausahawan baru terutama bagi pemuda dan perempuan</p>
          <br>
          <p><b>Misi Garasi PUEM</b></p>
          <p>1. Memperkenalkan hasil produk produk kelompok usaha ekonomi masyarakat Jawa Timur kepada pasar / konsumen, dalam maupun luar Negeri <br>
            2. Meingkatkan kemampuan dan ketrampilanserta aksesibilitas bagi kelompok masyarakat<br>
            3. Meningkatkan pengetahuan dan ketrampilan dalam pengelolaan usaha dengan berbasis sumber daya potensi lokal<br>
            4. Menggali potensi lokal yang guna meningkatkan kesejahteraan masyarakat<br>
            5. Meningkatkan kemandirian dan pengembangan usaha ekonomi masyarakat yang produktif guna peningkatan kualitas hidup masyarakat</p>

        </div>
      </div>
    </div>
  </div>
</section>