<?php 
$url_base_image = $this->config->item('base_url_image');
?>

<br>
<small class="mt-4">Produk Berdasarkan <span class="badge badge-success text-white px-3 py-1"><?php echo($desa['description']) ?></span></small>
<div class="row">
  <?php foreach ($filterproduk as $prod) { ?>
  <div class="col-lg-2 col-md-3 col-sm-3 col-6 mb-3">
    <div class="box-products border rounded h-100">
      <a href="<?= base_url(); ?>home/detail/<?= $prod['idp']; ?>">
      	<div class="img-cover">
          <img class="img-fluids rounded-top d-inline-block" width="100%"  src="<?= $url_base_image.'userfile/'.$prod['kd_desa'].'/puem/'.$prod['foto_produk'] ?>" alt="">
       </div>
      </a>
      <div class="text-sm-left box-products-inner p-all-5">
        <div class="text-title-produk"><small class="text-info">
          <a href="<?= base_url(); ?>home/detail/<?= $prod['idp']; ?>"><b><?= $prod['nama']; ?></b></a>
        </small></div>
        <small class="">Rp <?= number_format((int) $prod['harga'], 2, ',','.') ?><span class="float-right star mt-2"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i></span></small>
        <ul class="list-inline text-dark">
          <?php foreach($this->Home_model->getAllKategoriByProduk($prod['idp']) as $data){ ?>
            <li class="list-inline-item kategori"><small style="font-size: 11px;"><?= $data['label']; ?></small></li><br>
          <?php } ?>
          <li class="list-inline-item">
            <small style="font-size: 11px;">
              <?= $prod['jml_view']; ?> <i class="fa fa-eye"></i>
            </small> 
          </li>
          <li class="list-inline-item">
            <small style="font-size: 11px;">

            <?php $buy = $this->Member_model->getJumlahBuy($prod['idp']); echo ($buy != null)?$buy['total']:0;
            ?>

           <i class="fa fa-shopping-cart"></i></small>
         </li>
        </ul>
      </div>
    </div>
  </div>
  <?php } ?>
</div>