<?php 
$url_base_image = $this->config->item('base_url_image');
?>
<section class="context-dark">
  <div class="parallax-container" data-parallax-img="<?= base_url(); ?>assets/tema/images/bg-header-1920x362.jpg">
    <div class="parallax-content">
      <div class="container section-top-34 section-bottom-34 text-center">
        <div><h3 class="font-weight-bold"><i class="fa fa-folder"></i> Registrasi Acara</h3></div>
        <ul class="list-inline list-inline-dashed p text-light breadcrumb-modern">
          <li class="list-inline-item active"><a href="<?= base_url();?>">Home</a></li>
          <li class="list-inline-item active"><a href="<?= base_url('pelatihan');?>">Pelatihan</a></li>
          <li class="list-inline-item"> Registrasi Acara</li>
        </ul>
      </div>
    </div>
  </div>
</section>

<section class="section-top-34 section-lg-top-66 content">
	<div class="container">
		<div class="row">
			<div class="col-md-8">
				<div class="card">
					<div class="card-body">
						<?php if($acara != null && count($acara) > 0){ ?>
              <div class="text-center">
							   <h4><b><?= $acara['judul'] ?></b></h4>
                  <img src="<?= $url_base_image.'userfile/puem/'.$acara['cover'] ?>" class="img-fluid" style="width:80%">
              </div>
              <h4><b>Deskripsi Kegiatan</b></h4>
							<?= $acara['deskripsi'] ?><br>
							Tanggal : <?= $acara['tanggal'] ?><br>
							
              <?php if($status_register_event == null): ?>
  							<br><br><h5><b>Pendaftar</b></h5>
  							<?php if($member_detail != null){ ?>
                  <?php //var_dump($member_detail) ?>
                  <form action="<?= base_url('member/register_event_process'); ?>" method="post">
                    <input type="hidden" class="form-control" id="acara_id" name="acara_id" required="true" readonly="true" value="<?= $acara['id']; ?>">
                    <input type="hidden" class="form-control" id="member_id" name="member_id" required="true" readonly="true" value="<?= $member_detail['id']; ?>">
                    <div class="form-group">
                      <label for="nama">Nama Lengkap</label>
                      <input type="text" class="form-control" id="nama" name="nama" required="true" readonly="true" value="<?= $this->session->ses_nama; ?>">
                    </div>
                    <button type="submit" name="add_profile" class="btn btn-info btn-sm float-right" >Daftar Acara <i class="fa fa-check"></i></button>
                  </form>
  							<?php } else { ?>
                  
                  <p>Untuk dapat melakukan pendaftaran, Mohon untuk dapat melengkapi data diri anda terlebih dahulu.</p>
                  <a href="<?= base_url('member/tambah') ?>" class="btn btn-block btn-danger">Lengkapi detail diri anda</a>
  							<?php } ?>
              <?php else: ?>
                <br><h4 class="text-center mt-3"><b>Anda Sudah Terdaftar <i class="fa fa-check text-success"></i></b></h4>
              <?php endif; ?>

						<?php } else { ?>
							<h3>Acara sudah selesai/tidak ditemukan</h3>
						<?php } ?>
					</div>
				</div>
			</div>
      <div class="col-md-4">
        
      </div>
		</div>
	</div>
</section>
<br><br>