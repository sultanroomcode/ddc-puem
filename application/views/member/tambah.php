<section class="context-dark">
  <div class="parallax-container" data-parallax-img="<?= base_url(); ?>assets/tema/images/bg-header-1920x362.jpg">
    <div class="parallax-content">
      <div class="container section-top-34 section-bottom-34 text-center">
        <div><h3 class="font-weight-bold"><i class="fa fa-user"></i> Informasi Data Pribadi</h3></div>
        <ul class="list-inline list-inline-dashed p text-light breadcrumb-modern">
          <li class="list-inline-item active"><a href="<?= base_url();?>">Home</a></li>
          <li class="list-inline-item"> Profil</li>
        </ul>
      </div>
    </div>
  </div>
</section>
<br><br>
<div class="container mb-3">
	<div class="row">
		<div class="col-md-12">
			<div class="card shadow">
	          <h4 class="text-center mt-3"><b>Tambah Detail Profil</b></h4>
	          <div class="card-body">
	            <form action="<?= base_url('member/tambah'); ?>" method="post">
	            	<?php echo validation_errors('<div class="error">', '</div>'); ?>
	              <div class="form-group">
	                <label for="nama">Nama Lengkap</label>
	                <input type="hidden" class="form-control" id="id" name="id" required="true" readonly="true" value="<?= $this->session->ses_id; ?>">
	                <input type="text" class="form-control" id="nama" name="nama" required="true" readonly="true" value="<?= $this->session->ses_nama; ?>">
	              </div>
	              <div class="form-group">
	                <label for="email">Email</label>
	                <input type="text" class="form-control" id="email" name="email" required="true" readonly="true" value="<?= $this->session->ses_email; ?>" >
	              </div>
	              <div class="form-group">
	                <label for="alamat">Alamat</label>
	                <textarea type="text" class="form-control" id="alamat" name="alamat" required="true"></textarea>
	              </div>
	              <div class="form-group">
	              	<label for="provinsi_selects">Pilih Provinsi</label>
	              	<select name="provinsi" id='provinsi_selects' onchange="filterKabupaten(this.value)">
			           <option>-- Pilih Provinsi --</option>
			           <?php
			           foreach($provinsi as $prop){
			             echo "<option value='".$prop['id']."'>".$prop['nama']."</option>";
			           }
			           ?>
			        </select>
			      </div>
			      <div class="form-group">
			        <label for="kota_selects">Pilih Kabupaten</label>
	              	<select name="kota" id='kota_selects' onchange="filterKecamatan(this.value)"></select>
	              </div>
	              <div class="form-group">
	              	<label for="kecamatan_selects">Pilih Kecamatan</label>
	              	<select name="kecamatan" id='kecamatan_selects' onchange="filterDesa(this.value)"></select>
	              </div>
	              <div class="form-group">
	              	<label for="kelurahan_selects">Pilih Kelurahan</label>
	              	<select name="kelurahan" id='kelurahan_selects'></select>

	              </div>
	              <div class="form-group">
	                <label for="tempat_lahir">Tempat Lahir</label>
	                <input type="text" class="form-control" id="tempat_lahir" name="tempat_lahir" required="true">
	              </div>
	              <div class="form-group">
	                <label for="tgl_lahir">Tanggal Lahir</label>
	                <input type="text" class="form-control" id="tgl_lahir" name="tgl_lahir" required="true" data-provide="datepicker" data-date-format="dd-mm-yyyy">
	              </div>
	              <div class="form-group">
	                <label for="jenis_kelamin">Jenis Kelamin</label>
	                <select class="form-control" id="jenis_kelamin" name="jenis_kelamin">
	                  <option value="L">Laki - laki</option>
	                  <option value="P">Perempuan</option>
	                </select>
	              </div>
	              <div class="form-group">
	                <label for="agama">Jenis Kelamin</label>
	                <select class="form-control" id="agama" name="agama">
	                  <option value="islam">Islam</option>
	                  <option value="kristen">Kristen</option>
	                  <option value="katolik">Katolik</option>
	                  <option value="hindu">Hindu</option>
	                  <option value="buddha">Buddha</option>
	                  <option value="konghucu">Kong Hu Cu</option>
	                </select>
	              </div>
	              <div class="form-group">
	                <label for="pekerjaan">Pekerjaan</label>
	                <input type="text" class="form-control" id="pekerjaan" name="pekerjaan" required="true">
	              </div>
	              
	              <div class="form-group">
	                <label for="telepon">Nomor Telepon</label>
	                <input type="text" class="form-control" id="telepon" name="telepon" required="true">
	              </div>
	              <button type="submit" name="tambah" class="btn btn-success btn-sm float-right" >Selesai <i class="fa fa-check"></i></button>
	               <a href="<?= base_url('member/profile'); ?>" class="btn btn-sm btn-outline-info float-right mx-2" >Batal</a>
	            </form>
	          </div>
	        </div>
		</div>
	</div>
</div>
<script type="text/javascript">
	function filterKabupaten(id_provinsi)
	{
		$('#kota_selects').load('<?= base_url() ?>wilayah/filter_kota/'+id_provinsi);
	}

	function filterKecamatan(id_kota)
	{
		$('#kecamatan_selects').load('<?= base_url() ?>wilayah/filter_kecamatan/'+id_kota);
	}

	function filterDesa(id_kecamatan)
	{
		$('#kelurahan_selects').load('<?= base_url() ?>wilayah/filter_kelurahan/'+id_kecamatan);
	}

</script>