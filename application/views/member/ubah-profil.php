<section class="context-dark">
  <div class="parallax-container" data-parallax-img="<?= base_url(); ?>assets/tema/images/bg-header-1920x362.jpg">
    <div class="parallax-content">
      <div class="container section-top-34 section-bottom-34 text-center">
        <div><h3 class="font-weight-bold"><i class="fa fa-user"></i> Informasi Data Pribadi</h3></div>
        <ul class="list-inline list-inline-dashed p text-light breadcrumb-modern">
          <li class="list-inline-item active"><a href="<?= base_url();?>">Home</a></li>
          <li class="list-inline-item"> Profil</li>
        </ul>
      </div>
    </div>
  </div>
</section>
<br><br>
<div class="container mb-3">
	<div class="row">
		<div class="col-md-12">
			<div class="card shadow">
	          <h4 class="text-center mt-3"><b>Ubah Detail Profil</b></h4>
	          <div class="card-body">
	            <form action="<?= base_url('member/ubah'); ?>" method="post">
	            	<?php echo validation_errors('<div class="error">', '</div>'); ?>
	              <div class="form-group">
	                <label for="nama">Nama Lengkap</label>
	                <input type="hidden" class="form-control" id="id" name="id" required="true" readonly="true" value="<?= $this->session->ses_id; ?>">
	                <input type="text" class="form-control" id="nama" name="nama" required="true" readonly="true" value="<?= $this->session->ses_nama; ?>">
	              </div>
	              <div class="form-group">
	                <label for="email">Email</label>
	                <input type="text" class="form-control" id="email" name="email" required="true" readonly="true" value="<?= $this->session->ses_email; ?>" >
	              </div>
	              <div class="form-group">
	                <label for="alamat">Alamat</label>
	                <input type="text" class="form-control" id="alamat" name="alamat" required="true" value="<?= $profil['alamat']; ?>">
	              </div>
	              <div class="form-group">
	                <label for="tempat_lahir">Tempat Lahir</label>
	                <input type="text" class="form-control" id="tempat_lahir" name="tempat_lahir" required="true" value="<?= $profil['tempat_lahir']; ?>">
	              </div>
	              <div class="form-group">
	                <label for="tgl_lahir">Tanggal Lahir</label>
	                <!-- data dr sql -> yyyy-mm-dd -->
	                <input type="text" class="form-control" id="tgl_lahir" name="tgl_lahir" required="true" data-provide="datepicker" data-date-format="dd-mm-yyyy" value="<?= date('d-m-Y', strtotime($profil['tgl_lahir'])); ?>">
	              </div>
	              <div class="form-group">
					<label for="jenis_kelamin">Jenis Kelamin</label>
					<select class="form-control" id="jenis_kelamin" name="jenis_kelamin">
						<option value="L" <?= ($profil['jenis_kelamin'] == 'L')?'selected':''?>> Laki - laki</option>
						<option value="P" <?= ($profil['jenis_kelamin'] == 'P')?'selected':''?>>Perempuan</option>
					</select>
				  </div>
	              <div class="form-group">
	                <label for="agama">Agama</label>
	                <select class="form-control" id="agama" name="agama">
	                  <option value="islam" <?= ($profil['agama'] == 'islam')?'selected':''?>>Islam</option>
	                  <option value="kristen" <?= ($profil['agama'] == 'kristen')?'selected':''?>>Kristen</option>
	                  <option value="katolik" <?= ($profil['agama'] == 'katolik')?'selected':''?>>Katolik</option>
	                  <option value="hindu" <?= ($profil['agama'] == 'hindu')?'selected':''?>>Hindu</option>
	                  <option value="buddha" <?= ($profil['agama'] == 'buddha')?'selected':''?>>Buddha</option>
	                  <option value="konghucu" <?= ($profil['agama'] == 'konghucu')?'selected':''?>>Kong Hu Cu</option>
	                </select>
	              </div>
	              <div class="form-group">
	                <label for="pekerjaan">Pekerjaan</label>
	                <input type="text" class="form-control" id="pekerjaan" name="pekerjaan" required="true" value="<?= $profil['pekerjaan']; ?>">
	              </div>
	              
	              <div class="form-group">
	                <label for="telepon">Nomor Telepon</label>
	                <input type="text" class="form-control" id="telepon" name="telepon" required="true" value="<?= $profil['telepon']; ?>">
	              </div>
	              <button type="submit" name="ubah" class="btn btn-success btn-sm float-right" >Selesai <i class="fa fa-check"></i></button>
	              <a href="<?= base_url('member/profile'); ?>" class="btn btn-sm btn-outline-info float-right mx-2" >Batal</a>
	            </form>
	          </div>
	        </div>
		</div>
	</div>
</div>