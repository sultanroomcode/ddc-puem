<?php 
$url_base_image = $this->config->item('base_url_image');
?>
<section class="context-dark">
  <div class="parallax-container" data-parallax-img="<?= base_url(); ?>assets/tema/images/bg-header-1920x362.jpg">
    <div class="parallax-content">
      <div class="container section-top-34 section-bottom-34 text-center">
        <div><h3 class="font-weight-bold"><i class="fa fa-shopping-cart"></i> Keranjang Belanja</h3></div>
        <ul class="list-inline list-inline-dashed p text-light breadcrumb-modern">
          <li class="list-inline-item active"><a href="<?= base_url();?>">Home</a></li>
          <li class="list-inline-item"><a href="<?= base_url();?>#produk">Produk</a></li>
          <li class="list-inline-item"> Keranjang Belanja</li>
        </ul>
      </div>
    </div>
  </div>
</section>

<section class="section-top-34 section-lg-top-66 content">
	<div class="container">
		<div class="row">
			<div class="col-md-8">
				<div class="card mb-4">
					<div class="card-body">							
						<div class="row">
							<?php //var_dump($product) ?>
							<div class="col-md-2 col-sm-3 col-3"><img class="img-fluid rounded" src="<?= $url_base_image.'userfile/'.$product['kd_desa'].'/puem/'.$product['foto_produk'] ?>">
              </div>
							<div class="col-md-7 col-sm-7 col-8">
								<p><b><?= $product['nama'] ?></b><br>
                  <!-- <small class="text-muted"><?= $product['deskripsi'] ?></small><br> -->
                </p>
                <p>Rp. <?= number_format((int) $product['harga'], 2, ',','.') ?></p>
                <div class="input-group">
                  <div class="input-group-prepend">
                    <div class="input-group-text">
                    <input type="number" id="change_value" name="change_value" min="1" onchange="ikutChange(this.value)" value="1">
                    </div>
                  </div>
                </div>
							</div>
						</div>
            <hr>
						<br>
					</div>
				</div>
			</div>
      <div class="col-md-4">
        <div class="card">
          <div class="card-body">
            <p><b>Ringkasan Belanja</b></p><hr>

            <br><hr>
            <h6>Total Biaya : <b class="float-right">Rp <span id="total_display"><?= number_format((int) $product['harga'], 2, ',','.') ?></span></b></h6>
            <br>
            <!-- <h3>User Profile</h3> -->

            <?php if($member_detail != null){ ?>

              <form action="<?= base_url('member/buy_process'); ?>" method="post">
                <input type="hidden" class="form-control" id="idp" name="idp" required="true" readonly="true" value="<?= $product['idp']; ?>">
                <input type="hidden" class="form-control" id="kd_desa" name="kd_desa" required="true" readonly="true" value="<?= $product['kd_desa']; ?>">
                <input type="hidden" class="form-control" id="kd_toko" name="kd_toko" required="true" readonly="true" value="<?= $product['kd_toko']; ?>">
                <input type="hidden" class="form-control" id="nama_produk" name="nama_produk" required="true" readonly="true" value="<?= $product['nama']; ?>">
                <input type="hidden" class="form-control" id="foto_produk" name="foto_produk" required="true" readonly="true" value="<?= $product['foto_produk']; ?>">
                <input type="hidden" class="form-control" id="harga" name="harga" required="true" readonly="true" value="<?= $product['harga']; ?>">
                <input type="hidden" class="form-control" id="kuantitas" name="kuantitas" required="true" readonly="true" value="1">
                <input type="hidden" class="form-control" id="total" name="total" required="true" readonly="true" value="<?= $product['harga'] ?>">
                <input type="hidden" class="form-control" id="member_id" name="member_id" required="true" readonly="true" value="<?= $member_detail['id']; ?>">

                <div class="form-group">
                  <label for="nama">Pembeli</label>
                  <input type="text" class="form-control" id="nama" name="nama" required="true" readonly="true" value="<?= $this->session->ses_nama; ?>">
                </div>
                <button type="submit" name="add_profile" class="btn btn-danger btn-sm float-right" >beli sekarang<i class="fa fa-check"></i></button>
              </form>
            <?php } else { ?>
              <div class="text-center">
                <p>Untuk melakukan pembelian, Mohon mengisi Detail Profil Anda</p>
                 <a href="<?= base_url('member/tambah') ?>" class="btn btn-info btn-sm">Tambah Detail Profil</a>
              </div>
            <?php } ?>
          </div>
        </div>
      </div>
		</div>
	</div>
</section>
<br><br>
<script type="text/javascript">
function numberWithCommas(x) {
    var parts = x.toString().split(",");
    parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ".");
    return parts.join(",");
}

function ikutChange(v) {
  $('#kuantitas').val(v);
  let total = v * $('#harga').val();
  $('#total').val(total);
  console.log(total);
  $('#total_display').html(numberWithCommas(total));
}
</script>

<!-- Modal add profile -->
  <!-- <div class="modal fade mt-3" id="ModalAddProfile" tabindex="-1" role="dialog" aria-labelledby="ModalAddProfile" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="text-center" id="ModalAddProfile"></h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
         
        <div class="modal-body">
          <h4 class="text-center"><b>Tambah Detail Profil</b></h4>
          <div class="card-body">
            <form action="<?= base_url('member/add_profile'); ?>" method="post">
              <div class="form-group">
                <label for="nama">Nama Lengkap</label>
                <input type="text" class="form-control" id="nama" name="nama" required="true" readonly="true" value="<?= $this->session->ses_nama; ?>">
              </div>
              <div class="form-group">
                <label for="email">Email</label>
                <input type="text" class="form-control" id="email" name="email" required="true" readonly="true" value="<?= $this->session->ses_email; ?>" >
              </div>
              <div class="form-group">
                <label for="alamat">Alamat</label>
                <input type="text" class="form-control" id="alamat" name="alamat" required="true">
              </div>
              <div class="form-group">
                <label for="tempat_lahir">Tempat Lahir</label>
                <input type="text" class="form-control" id="tempat_lahir" name="tempat_lahir" required="true">
              </div>
              <div class="form-group">
                <label for="tgl_lahir">Tanggal Lahir</label>
                <input type="text" class="form-control" id="tgl_lahir" name="tgl_lahir" required="true" data-provide="datepicker" data-date-format="dd-mm-yyyy">
              </div>
              <div class="form-group">
                <label for="jenis_kelamin">Jenis Kelamin</label>
                <select class="form-control" id="jenis_kelamin" name="jenis_kelamin">
                  <option value="L">Laki - laki</option>
                  <option value="P">Perempuan</option>
                </select>
              </div>
              <div class="form-group">
                <label for="agama">Jenis Kelamin</label>
                <select class="form-control" id="agama" name="agama">
                  <option value="islam">Islam</option>
                  <option value="kristen">Kristen</option>
                  <option value="katolik">Katolik</option>
                  <option value="hindu">Hindu</option>
                  <option value="budha">Budha</option>
                  <option value="konghucu">Kong Hu Cu</option>
                </select>
              </div>
              <div class="form-group">
                <label for="pekerjaan">Pekerjaan</label>
                <input type="text" class="form-control" id="pekerjaan" name="pekerjaan" required="true">
              </div>
              
              <div class="form-group">
                <label for="telepon">Nomor Telepon</label>
                <input type="text" class="form-control" id="telepon" name="telepon" required="true">
              </div>
              <button type="submit" name="add_profile" class="btn btn-success btn-sm float-right" >Selesai <i class="fa fa-check"></i></button>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div> -->