<section class="context-dark">
  <div class="parallax-container" data-parallax-img="<?= base_url(); ?>assets/tema/images/bg-header-1920x362.jpg">
    <div class="parallax-content">
      <div class="container section-top-34 section-bottom-34 text-center">
        <div><h3 class="font-weight-bold"><i class="fa fa-user"></i> Login</h3></div>
        <ul class="list-inline list-inline-dashed p text-light breadcrumb-modern">
          <li class="list-inline-item active"><a href="<?= base_url();?>">Home</a></li>
          <li class="list-inline-item"> Login Member</li>
        </ul>
      </div>
    </div>
  </div>
</section>
<br><br>
<div class="container mb-3">
  <div class="row justify-content-md-center">
    <div class="col-md-8">

      <?php if ( $this->session->flashdata('flash') ) : ?>
        <div class="row mt-3">
          <div class="col-md-12">
            <div class="alert alert-success alert-dismissible fade show" role="alert">Akun Anda <strong>BERHASIL </strong><?= $this->session->flashdata('flash'); ?> <br> Silahkan buka Email Anda untuk melakukan Aktivasi
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
            </div>
          </div>
        </div> 
        <?php endif; ?>
      <?= $this->session->flashdata('message'); ?>
      <br>
      <div class="card shadow">
          <h3 class="text-center mt-3"><b><?= $judul; ?></b></h3>
          <div class="card-body">
            <small><?= $judul2; ?></small>
            <form action="<?= base_url(); ?>member/login" method="post" class="mb-4">
              <small class="text-danger" role="alert"><?php echo validation_errors('<div class="error">', '</div>'); ?></small><br>
              <div class="form-group">
                <label for="email">Email</label>
                <input type="text" class="form-control" id="email" name="email" required="true" value="<?= set_value('email'); ?>" >
              </div>
              <div class="form-group">
                <label for="password">Password</label>
                <input type="password" class="form-control" id="password" name="password" required="true">
              </div>
              <br>
              <button type="submit" class="btn btn-info btn-block btn-sm" >Login <i class="fa fa-sign-in"></i></button>
               <p>Belum punya Akun ?  <a href="<?= base_url('member/daftar'); ?>" class="text-info">Daftar Sekarang</a></p>
           </form>
         </div>
      </div>
    </div>
  </div>
</div>
