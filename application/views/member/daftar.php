<section class="context-dark">
  <div class="parallax-container" data-parallax-img="<?= base_url(); ?>assets/tema/images/bg-header-1920x362.jpg">
    <div class="parallax-content">
      <div class="container section-top-34 section-bottom-34 text-center">
        <div><h3 class="font-weight-bold"><i class="fa fa-user"></i> Informasi Data Pribadi</h3></div>
        <ul class="list-inline list-inline-dashed p text-light breadcrumb-modern">
          <li class="list-inline-item active"><a href="<?= base_url();?>">Home</a></li>
          <li class="list-inline-item"> Profil</li>
        </ul>
      </div>
    </div>
  </div>
</section>
<br><br>
<div class="container mb-3">
	<div class="row justify-content-md-center">
		<div class="col-md-8">
			<div class="card shadow">
	          	<h3 class="text-center mt-3"><b>DAFTAR</b></h3>
		        <div class="card-body">
		            <form action="<?= base_url('member/daftar'); ?>" method="post">
		              
		              <small class="text-danger" role="alert"><?php echo validation_errors('<div class="error">', '</div>'); ?></small><br>
		              <input type="hidden" name="image" value="default.png">
		              <input type="hidden" name="level" value="1">
		              <input type="hidden" name="user_access" value="1">
		              <input type="hidden" name="is_aktif" value='0'>
		              <div class="form-group">
		                <label for="nama">Nama Lengkap</label>
		                <input type="text" class="form-control" id="nama" name="nama" required="true" placeholder="masukkan nama" 
		                value="<?= set_value('nama'); ?>">
		              </div>
		              <div class="form-group">
		                <label for="email">Email</label>
		                <input type="text" class="form-control" id="email" name="email" required="true" placeholder="masukkan email" value="<?= set_value('email'); ?>">
		              </div>
		              <div class="form-group">
		                <label for="password1">Password</label>
		                <input type="password" class="form-control" id="password1" name="password1" placeholder="masukkan password" required="true">
		              </div>
		              <div class="form-group">
		                <label for="password2">Ulangi Password</label>
		                <input type="password" class="form-control" id="password2" name="password2" placeholder="ulangi password" required="true">
		              </div>
		              <button type="submit" name="daftar" class="btn btn-success btn-sm float-right" >Daftar Selesai <i class="fa fa-check"></i></button>
		              <p>Sudah punya Akun ?  <a href="<?= base_url('member/login'); ?>" class="text-info">Login Sekarang</a></p>
		            </form>
		        </div>
	        </div>
		</div>
	</div>
</div>