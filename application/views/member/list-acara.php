<section class="context-dark">
  <div class="parallax-container" data-parallax-img="<?= base_url(); ?>assets/tema/images/bg-header-1920x362.jpg">
    <div class="parallax-content">
      <div class="container section-top-34 section-bottom-34 text-center">
        <div><h3 class="font-weight-bold"><i class="fa fa-user"></i> Acara</h3></div>
        <ul class="list-inline list-inline-dashed p text-light breadcrumb-modern">
          <li class="list-inline-item active"><a href="<?= base_url();?>">Home</a></li>
          <li class="list-inline-item"> Acara</li>
        </ul>
      </div>
    </div>
  </div>
</section>


<div class="container">
  <?php if ( $this->session->flashdata('flash') ) : ?>
    <div class="row mt-3">
      <div class="col-md-6">
        <div class="alert alert-success alert-dismissible fade show" role="alert">Anda telah <strong>berhasil </strong><?= $this->session->flashdata('flash'); ?>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        </div>
      </div>
    </div> 
    <?php endif; ?>
</div>
<section class="section-top-34 section-lg-top-66 content">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="card">
					<div class="card-body">
						<h5><b>Anda telah mengikuti Acara</b></h5><br>
            
                        <table class="table">
                        <tr>
                            <th>Nama Acara</th>
                            <th>Tanggal</th>
                            <th>Tanggal Daftar</th>
                        </tr>
                        <?php foreach ($list_acara as $acara) { ?>
                        <tr>
                            <td><?= $acara['judul']; ?></td>
                            <td><?= $acara['tanggal']; ?></td>
                            <td><?= $acara['tanggal_daftar']; ?></td>
                        </tr>
                        <?php } ?>
                        </table>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>