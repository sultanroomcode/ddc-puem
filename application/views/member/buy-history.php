<?php 
$url_base_image = $this->config->item('base_url_image');
?>

<section class="context-dark">
  <div class="parallax-container" data-parallax-img="<?= base_url(); ?>assets/tema/images/bg-header-1920x362.jpg">
    <div class="parallax-content">
      <div class="container section-top-34 section-bottom-34 text-center">
        <div><h3 class="font-weight-bold"><i class="fa fa-user"></i> Pembelian</h3></div>
        <ul class="list-inline list-inline-dashed p text-light breadcrumb-modern">
          <li class="list-inline-item active"><a href="<?= base_url();?>">Home</a></li>
          <li class="list-inline-item"> Pembelian</li>
        </ul>
      </div>
    </div>
  </div>
</section>


<div class="container">
  <?php if ( $this->session->flashdata('flash') ) : ?>
    <div class="row mt-3">
      <div class="col-md-6">
        <div class="alert alert-success alert-dismissible fade show" role="alert">Anda telah <strong>berhasil </strong><?= $this->session->flashdata('flash'); ?>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        </div>
      </div>
    </div> 
    <?php endif; ?>
</div>
<section class="section-top-34 section-lg-top-66 content mb-4">
	<div class="container">
		<h5><b>Anda telah melakukan Pembelian</b></h5>     
      <div class="row">
         <?php foreach ($transaksi as $trans) { ?>
           <div class="col-lg-2 col-md-3 col-sm-3 col-6 mb-3">
             <div class="box-products border rounded h-100">
               <div class="img-cover">
                 <img class="img-fluids rounded-top d-inline-block" width="100%"  src="<?= $url_base_image.'userfile/'.$trans['kd_desa'].'/puem/'.$trans['foto_produk'] ?>" alt="">
               </div>
               <div class="text-sm-left box-products-inner p-all-5">
                  <div class="text-title-produk">
                     <small class="text-info">
                        <a href="<?= base_url(); ?>home/detail/<?= $trans['idp']; ?>">
                           <b><?= $trans['nama']; ?></b>
                        </a>
                     </small>
                  </div>
                  <small>Rp <?= number_format((int) $trans['harga'], 2, ',','.') ?>
                     <span class="float-right star mt-2"> Qty <?= $trans['kuantitas']; ?></span>
                  </small><br>
                  <small class="text-muted text-10">Total 
                     <p class="float-right mt-1">Rp <?= (int) $trans['harga'] * $trans['kuantitas']; ?>
                     </p>
                  </small><br>
                  <small class="text-10">
                     Transaksi <?= date('d F Y', strtotime($trans['created_at'])); ?>
                  </small>
                  <span class='wax-area btn btn-info btn-xs mt-2' data-tipped-options="ajax: {data: { id: '<?= $trans['idp'] ?>' }}">Info Toko</span>
               </div>
             </div>
           </div>
           <?php } ?>
         </div>	
	</div>
</section><br><br>