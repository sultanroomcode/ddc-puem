<section class="context-dark">
  <div class="parallax-container" data-parallax-img="<?= base_url(); ?>assets/tema/images/bg-header-1920x362.jpg">
    <div class="parallax-content">
      <div class="container section-top-34 section-bottom-34 text-center">
        <div><h3 class="font-weight-bold"><i class="fa fa-user"></i> Profile Pengguna</h3></div>
        <ul class="list-inline list-inline-dashed p text-light breadcrumb-modern">
          <li class="list-inline-item active"><a href="<?= base_url();?>">Home</a></li>
          <li class="list-inline-item"> Profile Pengguna</li>
        </ul>
      </div>
    </div>
  </div>
</section>

<div class="container">
  <?php if ( $this->session->flashdata('flash') ) : ?>
    <div class="row mt-3">
      <div class="col-md-6">
        <div class="alert alert-success alert-dismissible fade show" role="alert">Data Detail Profil <strong>berhasil </strong><?= $this->session->flashdata('flash'); ?>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        </div>
      </div>
    </div> 
    <?php endif; ?>
</div>

<section class="section-top-34 section-lg-top-66 content">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="card">
					<div class="card-body">

            <?php foreach ($profil_member as $m) { ?>
              <div class="row">
                <div class="col-md-4">
                  <img src="<?= base_url(); ?>assets/image/user.jpeg" width="100%">
                </div>
                <div class="col-md-6">
                  <h3><b><?= $m['nama']; ?></b></h3>
                  <p>Email : <?= $m['email']; ?></p>
                  <!-- <p>Alamat : <?= $m['provinsi']; ?></p> -->
                  <p>Jenis Kelamin : <?= $m['jenis_kelamin']; ?></p>
                  <p>Pekerjaan : <?= $m['pekerjaan']; ?></p>
                </div>
              </div>
            <?php } ?>

            <?php if($profil_member == null ) { ?>
                <div class="text-center">
                  <p>Anda belum mengisi Detail Profil</p>
                  <a href="<?= base_url('member/tambah'); ?>" class="btn btn-info btn-xs" >Tambah Detail</a></div>
              <?php } else { ?>
                  <div class="text-center"><a href="<?= base_url('member/ubah'); ?>" class="btn 
                    btn-info btn-xs disabled" >Ubah Detail</a></div>
              <?php } ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<br><br>