<?php 
 
/**
 * Member Controller
 */
class Member extends CI_Controller {
	public $db2;
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Member_model');
		$this->load->model('Cart_model');
		$this->load->model('Home_model');
		$this->load->model('Wilayah_model');
		$this->load->library('form_validation');
		// $this->db2 = $this->load->database('db2', true);
		
		// jika tidak administrator
		/*if ($this->session->userdata('user_access') != 1){
			redirect('user');
		}*/
	}

	public function index ()
	{
		redirect('member/profile');
	}

	// daftar member baru
	public function daftar()  
	{
		// if ( $this->session->userdata('masuk') == TRUE ) {
		// 	redirect('/');
		// }
		
		$data['judul'] = 'Form Daftar Member Baru';
		$this->form_validation->set_rules('nama', 'Nama', 'required|trim');
		$this->form_validation->set_rules('email', 'Email', 'required|trim|valid_email|is_unique[member.email]',[
			'valid_email' => 'Email tidak valid',
			'is_unique' => 'Email sudah terdaftar'
		]);
		$this->form_validation->set_rules('password1', 'Password', 'required|trim|min_length[8]|matches[password2]',[
			'matches' => 'Password tidak sama!',
			'min_length' => 'Password minimal 8 karakter'
		]);
		$this->form_validation->set_rules('password2', 'Password', 'required|trim|matches[password1]');

		if ( $this->form_validation->run() == FALSE ) {
			$this->load->view('templates/header', $data);
			$this->load->view('member/daftar', $data);
			$this->load->view('templates/footer');
		} else {
			// siapkan token
			$token = base64_encode(random_bytes(32));

			$this->Member_model->tambahDataToken($token);
			$this->Member_model->_sendEmail($token, 'verify');
			$this->Member_model->tambahDataMember();

			$this->session->set_flashdata('flash', 'terdaftar');
			redirect('member/login');
		}
	}

	public function verify()
	{
		$email = $this->input->get('email');
		$token = $this->input->get('token');
		$member = $this->Member_model->member($email);
		if ($member) { 

			if ($member) {
				$member_token = $this->Member_model->verifyIt($token);
				if($member_token) {
					if (time() - $member_token['date_created'] < ($this->config->item('email_smtp_header')['expired_time'])) { 
						$this->Member_model->setToActive($email);
						$this->Member_model->deleteTokenByEmail($email);
						$this->session->set_flashdata('message', '<div class="alert alert-success" role="alert" >Akun Anda sudah BERHASIL di Aktivasi. Silahkan Login</div>');
					} else {
						// $this->db2->delete('member', ['email' => $email]);
						$this->Member_model->deleteTokenByEmail($email);
						$this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert" >Token sudah Expired</div>');
					}
				} else {
					$this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert" >Aktifasi Akun Gagal! Token tidak valid</div>');
				}
			}
		} else {
			$this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert" >Aktifasi Akun Gagal! . Email Salah</div>');
		}

		redirect('member/login');
	}

	public function test_email() 
	{	
		var_dump($this->Member_model->testEmail('agussutarom@gmail.com'));
	}

	// login
	public function login() 
	{
		// if ( $this->session->userdata('masuk') == TRUE ) {
		// 	redirect('/');
		// }

		$data['judul'] = 'Login';
		$data['judul2'] = '';

		$this->form_validation->set_rules('email', 'Email', 'required|trim|valid_email',[
			'valid_email' => 'Email tidak valid',
			'is_unique' => 'Email sudah terdaftar'
		]);
		$this->form_validation->set_rules('password', 'Password', 'trim|required');

		if ($this->form_validation->run() == false) { 
			$this->load->view('templates/header', $data);
			$this->load->view('member/login');
			$this->load->view('templates/footer');
		} else {
			$this->auth();
		}
	}


	// pengecekan login
	public function auth(){
        $email = htmlspecialchars($this->input->post('email',TRUE),ENT_QUOTES);
        $password = htmlspecialchars($this->input->post('password',TRUE),ENT_QUOTES);
        $cek_member = $this->Member_model->auth_member($email,$password);
 
        if($cek_member->num_rows() > 0) { //jika login 
                $data=$cek_member->row_array();
                $this->session->set_userdata('masuk',TRUE);
                if($data['is_aktif'] =='1') {
                	$this->session->set_userdata('akses','1');
                    $this->session->set_userdata('ses_id',$data['id']);
                    $this->session->set_userdata('ses_nama',$data['nama']);
                    $this->session->set_userdata('ses_email',$data['email']);
                    redirect('/');
                } else {
                	$data['judul'] = 'Login Gagal! Akun Belum di Aktivasi';
                	$data['judul2'] = 'Cek Email Anda untuk melakukan Aktivasi';
		            $this->load->view('templates/header', $data);
					$this->load->view('member/login');
					$this->load->view('templates/footer');
                }
 
        } else { //jika login gagal

        	$data['judul'] = 'Login Gagal';
        	$data['judul2'] = 'Email / Password Anda Salah';
            $this->load->view('templates/header', $data);
			$this->load->view('member/login');
			$this->load->view('templates/footer');
        }
    }



    // halaman profil
    public function profile()
	{
		if ( $this->session->userdata('masuk') != TRUE ) {
			redirect('/');
		}

		$data['judul'] = 'Profile Pengguna';
		$data['profil_member'] = $this->Member_model->member_detail($this->session->userdata('ses_email'));
		$this->load->view('templates/header', $data);
		$this->load->view('member/profile', $data);
		$this->load->view('templates/footer');
	}

	// tambah detal profil
	public function tambah()
	{
		if ( $this->session->userdata('masuk') != TRUE ) {
			redirect('/');
		}

		$data['judul'] = 'Tambah Detail Profil';
		$data['provinsi'] = $this->Wilayah_model->getProvinsi();
		
		$this->form_validation->set_rules('id', 'ID', 'required');
		$this->form_validation->set_rules('nama', 'Nama', 'required');
		$this->form_validation->set_rules('alamat', 'Alamat', 'required');
		$this->form_validation->set_rules('provinsi', 'Provinsi', 'required');
		$this->form_validation->set_rules('kota', 'Kota / Kabupaten', 'required');
		$this->form_validation->set_rules('kecamatan', 'Kecamatan', 'required');
		$this->form_validation->set_rules('kelurahan', 'Kelurahan / Desa', 'required');
		$this->form_validation->set_rules('tempat_lahir', 'Tempat lahir', 'required');
		$this->form_validation->set_rules('tgl_lahir', 'Tanggal lahir', 'required');
		$this->form_validation->set_rules('jenis_kelamin', 'Jenis Kelamin', 'required');
		$this->form_validation->set_rules('agama', 'Agama', 'required');
		$this->form_validation->set_rules('pekerjaan', 'Pekerjaan', 'required');
		$this->form_validation->set_rules('email', 'Email', 'required');
		$this->form_validation->set_rules('telepon', 'Telepon', 'required');

		if ( $this->form_validation->run() == FALSE ) {
			$this->load->view('templates/header', $data);
			$this->load->view('member/tambah', $data);
			$this->load->view('templates/footer');
		} else {
			$this->Member_model->tambahDataProfil();
			$this->session->set_flashdata('flash', 'ditambahkan');
			redirect('member/profile');
		}
	}

	// ubah detail profil
	public function ubah()
	{
		if ( $this->session->userdata('masuk') != TRUE ) {
			redirect('/');
		}

		$data['judul'] = 'Ubah Detail Profil';
		
		$this->form_validation->set_rules('id', 'ID', 'required');
		$this->form_validation->set_rules('nama', 'Nama', 'required');
		$this->form_validation->set_rules('alamat', 'Alamat', 'required');
		$this->form_validation->set_rules('tempat_lahir', 'Tempat lahir', 'required');
		$this->form_validation->set_rules('tgl_lahir', 'Tanggal lahir', 'required');
		$this->form_validation->set_rules('jenis_kelamin', 'Jenis Kelamin', 'required');
		$this->form_validation->set_rules('agama', 'Agama', 'required');
		$this->form_validation->set_rules('pekerjaan', 'Pekerjaan', 'required');
		$this->form_validation->set_rules('email', 'Email', 'required');
		$this->form_validation->set_rules('telepon', 'Telepon', 'required');

		if ( $this->form_validation->run() == FALSE ) {
			$data['profil'] = $this->Member_model->check_member_detail($this->session->userdata('ses_id'));
			$this->load->view('templates/header', $data);
			$this->load->view('member/ubah-profil', $data);
			$this->load->view('templates/footer');
		} else {
			$this->Member_model->ubahDataProfil();
			$this->session->set_flashdata('flash', 'di Ubah');
			redirect('member/profile');
		}
	}

	//event register
	public function event_register($id)
	{
		if ( $this->session->userdata('masuk') != TRUE ) {
			redirect('/');
		}

		$session_id = $this->session->userdata('ses_id');

		$data['judul'] = 'Registrasi Acara';
		$data['acara'] = $this->Home_model->getPelatihanById($id);
		$data['status_register_event'] = $this->Member_model->getPelatihanStatus($id, $session_id);
		$data['member_detail'] = $this->Member_model->check_member_detail($session_id);
		
		$this->load->view('templates/header', $data);
		$this->load->view('member/event-register', $data);
		$this->load->view('templates/footer');
	}

	public function register_event_process()
	{
		if ( $this->session->userdata('masuk') != TRUE ) {
			redirect('/');
		}

		$data['judul'] = 'Tambah Profil';
		
		$this->form_validation->set_rules('acara_id', 'ID Acara', 'required');
		$this->form_validation->set_rules('member_id', 'ID Member', 'required');

		$id = (int) $this->input->post('acara_id', true);
		if ( $this->form_validation->run() == FALSE ) {
			$this->session->set_flashdata('flash', 'gagal registrasi acara');
			redirect('event-register/'.$id);
		} else {
			$this->Member_model->tambahDataRegistrasi();
			$this->session->set_flashdata('flash', 'berhasil registrasi acara');
			redirect('event-register/'.$id);
		}
	}

	//cart
	public function keranjang($id)

	{
		if ( $this->session->userdata('masuk') != TRUE ) {
			redirect('/');
		}
		
		$data['judul'] = 'Detail Data Keranjang Belanja';
		$data['product'] = $this->Home_model->getProdukById($id);
		$data['member_detail'] = $this->Member_model->check_member_detail($this->session->userdata('ses_id'));
		$data['profil_member'] = $this->Member_model->member_detail($this->session->userdata('ses_email'));
		
		$this->load->view('templates/header', $data);
		$this->load->view('member/keranjang-belanja', $data);
		$this->load->view('templates/footer');
	}

	public function buy_process()
	{
		if ( $this->session->userdata('masuk') != TRUE ) {
			redirect('/');
		}
		
		$this->form_validation->set_rules('idp', 'ID Produk', 'required');
		$this->form_validation->set_rules('kd_desa', 'Kode Desa', 'required');
		$this->form_validation->set_rules('kd_toko', 'Kode Toko', 'required');
		$this->form_validation->set_rules('nama_produk', 'Nama Produk', 'required');
		$this->form_validation->set_rules('foto_produk', 'Foto Produk', 'required');
		$this->form_validation->set_rules('harga', 'Harga Produk', 'required');
		$this->form_validation->set_rules('kuantitas', 'Kuantitas', 'required');
		$this->form_validation->set_rules('member_id', 'ID Member', 'required');

		if ( $this->form_validation->run() == FALSE ) {
			$this->session->set_flashdata('flash', 'Gagal beli');
			redirect('member/profile');
		} else {
			$this->Cart_model->memberBuy();
			$this->session->set_flashdata('flash', 'melakukan Pembelian');
			redirect('member/buy_history');
		}
	}

	public function buy_history()
	{
		$data['judul'] = 'Histori Belanja';
		$data['transaksi'] = $this->Cart_model->getListTransaksi($this->session->userdata('ses_id'));
		$this->load->view('templates/header', $data);
		$this->load->view('member/buy-history', $data);
		$this->load->view('templates/footer');
	}

	public function list_acara()
	{
		$data['judul'] = 'List Acara';
		$data['list_acara'] = $this->Member_model->getAcaraByPeserta($this->session->userdata('ses_id'));
		$this->load->view('templates/header', $data);
		$this->load->view('member/list-acara', $data);
		$this->load->view('templates/footer');
	}

	public function view_toko_by_id()
	{
		$data['judul'] = 'Data Toko';
		$idp = (int) $this->input->get('id', true);
		$data['toko'] = $this->Cart_model->getTokoByIdProduct($idp);
		$this->load->view('member/view-toko-by-id', $data);
	}

    function logout() {
        $this->session->sess_destroy();
        $url=base_url('/');
        redirect($url);
    }

}