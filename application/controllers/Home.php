<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller 
{
	public function __construct()
	{
		parent::__construct();

		$this->load->model('Home_model');
		$this->load->model('Member_model');
		$this->load->model('Wilayah_ddc_model');
		
	}

	public function index() 
	{
		$data['judul'] = 'DPMD Jatim - Garasi PUEM';

		$this->db->order_by('created_at', 'DESC');
		// $data['produk'] = $this->Home_model->getAllProduk();
		// $data['produkPop'] = $this->Home_model->getProdukPopular();
		$data['kategori'] = $this->Home_model->getKategoriAll();
		$data['pelatihan'] = $this->Home_model->getSidebarPelatihan();
		$data['kabupaten'] = $this->Wilayah_ddc_model->getKota();
		$this->load->view('templates/header', $data);
		$this->load->view('home/index', $data);
		$this->load->view('templates/footer');
	}

	public function about() 
	{
		$data['judul'] = 'DPMD Jatim - Tentang PUEM';
		
		$this->load->view('templates/header', $data);
		$this->load->view('home/about', $data);
		$this->load->view('templates/footer');
	}

	public function detail($idp) 
	{
		$data['judul'] = 'DPMD Jatim - Detail Produk';
		
		$data['produk'] = $this->Home_model->getProdukById($idp);
		$data['produk_photo'] = $this->Home_model->getAllPhoto($idp);
		$this->Home_model->addReadOne($idp);
		$this->load->view('templates/header', $data);
		$this->load->view('home/produk-detail', $data);
		$this->load->view('templates/footer');
	}

	public function getpicture($idp, $desa)
	{
		//get photo from produk
		$pro = $this->Home_model->getProdukById($idp);
		//get photo from foto_produk
		$res = $this->Home_model->getAllPhoto($idp);
		$arr = [];
		$url_base_image = $this->config->item('base_url_image');

		$arr[] = $url_base_image.'userfile/'.$desa.'/puem/'.$pro['foto_produk'];
		foreach($res as $v){
			$arr[] = $url_base_image.'userfile/'.$desa.'/puem/'.$v['src'];
		}

		echo json_encode($arr);
	} 

	public function kategori($slug)
	{
		//get photo from produk
		$data['judul'] = 'DPMD Jatim - kategori';
		$data['kategori'] = $this->Home_model->getKategoriAll();
		$data['kategori_product'] = $this->Home_model->getAllProdukByKategori($slug);

		$this->load->view('templates/header', $data);
		$this->load->view('home/kategori', $data);
		$this->load->view('templates/footer');
	} 


	public function detailMember($kd_desa,$kd_toko) 
	{
		$data['judul'] = 'DPMD Jatim - Detail Member';
		
		$data['member'] = $this->Home_model->getMemberById($kd_desa,$kd_toko);
		$data['contact'] = $this->Home_model->getContactById($kd_desa,$kd_toko);
		$data['prodmember'] = $this->Home_model->getAllProdukByMember($kd_desa,$kd_toko);

		$this->load->view('templates/header', $data);
		$this->load->view('home/detail-member', $data);
		$this->load->view('templates/footer');
	}

	public function daftar_ukm() 
	{
		$data['judul'] = 'DPMD Jatim - Daftar UKM';
		
		$data['member'] = $this->Home_model->getMemberAll();
		$this->load->view('templates/header', $data);
		$this->load->view('home/daftar_ukm', $data);
		$this->load->view('templates/footer');
	}

	public function info_pelatihan() 
	{
		$data['judul'] = 'DPMD Jatim - Info Pelatihan';
		
		$data['pelatihan'] = $this->Home_model->getPelatihanAll();
		$this->load->view('templates/header', $data);
		$this->load->view('home/info-pelatihan', $data);
		$this->load->view('templates/footer');
	}

	public function detail_pelatihan($id) 
	{
		$data['judul'] = 'DPMD Jatim - Detail Pelatihan';
		
		$data['pelatihan'] = $this->Home_model->getPelatihanById($id);
		$data['peserta']  = $this->Member_model->getPesertaAcara($id);
		$this->load->view('templates/header', $data);
		$this->load->view('home/pelatihan-detail', $data);
		$this->load->view('templates/footer');
	}

	public function video() 
	{
		$data['judul'] = 'DPMD Jatim - Video';
		
		$data['video'] = $this->Home_model->getVideoAll();
		$this->load->view('templates/header', $data);
		$this->load->view('home/video', $data);
		$this->load->view('templates/footer');
	}

	public function produk_api()
	{	
		$produk = $this->Home_model->getSearchProduk($this->input->get('term', TRUE));
		$arrproduk = [];
		foreach ($produk as $p) {
			$arrproduk[] = ["value" => $p['nama'],"label" => $p['nama'], "id" => $p['idp']];
		}		
		
		echo json_encode($arrproduk);
	}

	public function panduan() 
	{
		$data['judul'] = 'DPMD Jatim - Panduan Belanja';
		
		// $data['video'] = $this->Home_model->getVideoAll();
		$this->load->view('templates/header', $data);
		$this->load->view('home/panduan', $data);
		$this->load->view('templates/footer');
	}

	public function awal_produk() 
	{
		$data['produk'] = $this->Home_model->getAllProduk();
		$data['produkPop'] = $this->Home_model->getProdukPopular();
		$this->load->view('home/awal-produk',$data);
	}

	public function filter_produk($id_desa) 
	{
		$data['filterproduk'] = $this->Home_model->getAllProdukByIdDesa($id_desa);
		$data['desa'] = $this->Wilayah_ddc_model->getDesa($id_desa);
		$this->load->view('home/produk', $data);
	}

	public function galeri() 
	{
		$data['judul'] = 'DPMD Jatim - Galeri Belanja';
		$data['galeri'] = $this->Home_model->getGaleriAll();
		
		// $data['video'] = $this->Home_model->getVideoAll();
		$this->load->view('templates/header', $data);
		$this->load->view('home/galeri', $data);
		$this->load->view('templates/footer');
	}
}