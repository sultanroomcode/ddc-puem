<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Wilayah extends CI_Controller {

  public function filter_kota($id_provinsi){
    // load base_url 
    // $this->load->helper('url');
   
    //load model 
    $this->load->model('Wilayah_model');
   
    // get kota 
    $data['kota'] = $this->Wilayah_model->getKota($id_provinsi);
   
    // load view 
    $this->load->view('wilayah/filter_kota',$data); 
  }

  public function filter_kecamatan($id_kota){
    
    $this->load->model('Wilayah_model');
   
    $data['kecamatan'] = $this->Wilayah_model->getKecamatan($id_kota);
   
    // load view 
    $this->load->view('wilayah/filter_kecamatan',$data); 
  }

  public function filter_kelurahan($id_kecamatan){
    
    $this->load->model('Wilayah_model');
   
    $data['kelurahan'] = $this->Wilayah_model->getKelurahan($id_kecamatan);
   
    // load view 
    $this->load->view('wilayah/filter_kelurahan',$data); 
  }

  public function filter_kelurahan_nomor($id_kecamatan){
    
    $this->load->model('Wilayah_model');
   
    $data['kelurahan'] = $this->Wilayah_model->getKelurahan($id_kecamatan);
   
    // load view 
    $this->load->view('wilayah/filter_kelurahan_nomor',$data); 
  }





  //ddc
  public function ddc_filter_kecamatan($id_kota){
    
    $this->load->model('Wilayah_ddc_model');
   
    $data['kecamatan'] = $this->Wilayah_ddc_model->getKecamatan($id_kota);
   
    // load view 
    $this->load->view('wilayah/ddc_filter_kecamatan',$data); 
  }

  public function ddc_filter_kelurahan($id_kecamatan){
    
    $this->load->model('Wilayah_ddc_model');
   
    $data['kelurahan'] = $this->Wilayah_ddc_model->getKelurahan($id_kecamatan);
   
    // load view 
    $this->load->view('wilayah/ddc_filter_kelurahan',$data); 
  }
}