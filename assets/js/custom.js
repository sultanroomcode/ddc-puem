var base_url = $('body').attr('data-url');
$(document).ready(function(){
  //jika layar berubah maka update slick
  var browserWidth = $('body').width();
  console.log(browserWidth);
  var mobile = false;//desktop
  var slidesToShowVar = 6;
  if(browserWidth < 921)
  {
  	var mobile = true;//tab
  	var slidesToShowVar = 4;
  }
  if(browserWidth < 500)
  {
  	var mobile = true;//mobile
  	var slidesToShowVar = 2;
  }

  $('.slide-kategori').slick({
    slidesToShow: slidesToShowVar,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 2000,
  });

  $('#mymodalpop-up').modal('show');
});

// const flashData = $('.flash-data').data('flashdata');
  // console.log(flashData);
  // if ( flashData ) {
  //   Swal({
  //     title: 'Data',
  //     text: 'Berhasil' + flashData,
  //     type: 'success'
  //   });
  // }

// Swal({
//   title: 'Data',
//   text: 'Berhasil',
//   type: 'success'
// });

Tipped.create('.wax-area', {
    ajax: {
      url: base_url+'/member/view_toko_by_id/',
      type: 'get'
    },
    cache:false,
    skin: 'light',
    size: 'large',
    radius: false,
    position: 'topleft'
  });
